//////////////////////////////////
///    GalacticObjects         ///
/// ----------------------     ///
/// Objects whose inmediate    ///
/// parent is a galaxy         ///
/// ------------------------   ///
//////////////////////////////////

/**
 * @module Core
 */

/**
 * Objects whose inmediate parent is a galaxy
 *
 * @class GalacticObject
 * @constructor
 * @param name {String} Name of the galactic object
 * @param coords {Object} Galaxy coordinates {x,y}
 */
function GalacticObject(id, name, coords){
	this.id = id;
    this.coords = coords;
    this.name = name;
}

GalacticObject.prototype = {};

if(typeof module !== 'undefined' && typeof module.exports !== 'undefined')
	module.exports = GalacticObject;