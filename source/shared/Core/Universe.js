
///////////////////////////////
///       Universe          ///
/// ----------------------  ///
/// "If you wish to make    ///
/// an apple pie from       ///
/// scratch, you must first ///
/// invent the universe."   ///
///  -Carl Sagan            ///
///////////////////////////////

/**
* @module Core
*/
if(typeof module !== 'undefined' && typeof module.exports !== 'undefined')
    var GalacticObject = require ('./GalacticObject.js');

/**
* Represents the universe
*
* @class Universe
* @constructor
* @param Galaxy First universe's galaxy
*/
function Universe(galaxy)
{
    this.galaxies = [];
    if(galaxy)
        this.galaxies.push(galaxy);

    this.fleets= [];
}

Universe.prototype =
{

    /**
     * Adds a fleet to this galaxy
     *
     * @param {Fleet} fleet
     */
    addFleet: function(fleet){
        this.fleets.push(fleet);
    },

    /**
     * Removes a fleet from this galaxy
     *
     * @param {String} fleetId
     */
    removeFleet: function(fleetId){
        //TODO
    }
};

if(typeof module !== 'undefined' && typeof module.exports !== 'undefined')
    module.exports = Universe;

