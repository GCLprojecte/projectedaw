/////////////////////////////////////////////////////////
///                     Galaxy                        ///
/// ------------------------------------------------- ///
/// Objects whose immediate parent is the Universe    ///
/// ------------------------------------------------- ///
/////////////////////////////////////////////////////////

/**
 * @module Core
 */


/**
 * Objects whose inmediate parent is the Universe
 *
 * @class Galaxy
 * @constructor
 *
 * @param {int} Identifier
 * @param {String} Name of the galaxy
 * @param {String[]} Composition of the galaxy in elements
 * @param {String} Shape code (i.e. The milky way: 'SBc')
 * @param {int} Radius in Megaparsecs
 * @param {Object} Galaxy coords in the Universe
 * @param [galacticObjects={[]}] Objects inside the galaxy
 *
 * @requires GOD
 * @requires Util
 */
function Galaxy(id,name,type,radius,coords,composition,gObjs){
    this.id = id;
    this.name = name;
    this.composition = composition;
    this.type = type;
    this.radius = radius;
    this.coords = coords;
    this.fleets={};
    this.galacticObjects=gObjs || [];
}

Galaxy.prototype = {
    

    /**
     * Adds a fleet to this galaxy
     *
     * @method addFleet
     * @param Fleet Fleet to add
     */
    addFleet: function(fleet){
        this.fleets[fleet.id] = fleet;
    },

    /**
    * Generates a comma separated list of the composition
    *
    * @method getComposition
    * @return {String} Composition list
    */
    getComposition: function(){
        var composition = "";
        this.composition.forEach(function(element){
            composition += element + ", ";
        });

        composition = composition.substr(0, composition.length-1);

        return composition;
    },

    /**
     * Removes a fleet from this galaxy
     *
     * @method removeFleet
     * @param String Fleet id to remove
     */
    removeFleet: function(fleetId){
        delete this.fleets[fleetId];
    }
};

if(typeof module !== 'undefined' && typeof module.exports !== 'undefined')
    module.exports = Galaxy;
