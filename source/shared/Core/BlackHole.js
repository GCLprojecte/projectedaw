///////////////////////////////
///      BlackHole          ///
/// ----------------------  ///
/// Extends: GalacticObject ///
/// ----------------------- ///
/// Requires:               ///
/// -GalacticObject.js      ///
///////////////////////////////

/**
*@module Core
*/

/**
* Represents a black hole
*
* @class BlackHole
* @extends GalacticObject
*/
function BlackHole(id, name, coords, type, objects, eventHorizon){
    //Initialise super class
    GalacticObject.call(this, id, name, coords);

    this.eventHorizon = eventHorizon;
}


BlackHole.prototype = Object.create(GalacticObject.prototype,{

});

if(typeof module !== 'undefined' && typeof module.exports !== 'undefined')
	module.exports = BlackHole;