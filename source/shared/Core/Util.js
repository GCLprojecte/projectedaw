
//////////////////////////////
///         UTIL           ///
/// ---------------------- ///
/// Common functions of    ///
/// general use            ///
//////////////////////////////

//Singleton
var Util = {
    /**
     * Random id.
     * Like: "hrgri164"
     *
     * @returns {String}
     */
    genId: function(){
      //StackOverflow - broofa
      
      return 'ixyxx4xxx'.replace(/[xy]/g, function(c) {
        var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
        return v.toString(16);
      });

    },

    /**
    * Normal distribution
    *
    * @author protonfish.com
    */
    rnd_snd: function(){
          return (Math.random()*2-1)+(Math.random()*2-1)+(Math.random()*2-1);

    },
    normalDist: function(stdev,mean){
          return Math.round(this.rnd_snd()*stdev+mean);
    },

    randomInt: function(min,max){
        return Math.floor(Math.random() * (max - min + 1)) + min;
    },

    objToArray: function(obj){
        var arr = [];
        for(var index in obj) 
        { 
            if(obj.hasOwnProperty(index)) 
            {
                arr.push(index);
            }
        }

        return arr;
    },
    degToRad: function(deg){
      return deg * (Math.PI/180);
    }

};

if(typeof module !== 'undefined' && typeof module.exports !== 'undefined')
  module.exports = Util;