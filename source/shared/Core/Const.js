/////////////////////////////
///          Const         //
/// ---------------------- //
/// Constant values        //
/////////////////////////////

if(typeof module !== 'undefined' && typeof module.exports !== 'undefined')
	var THREE = require('three');

//Singleton
var Const = {
	
	ELEMENT: {
		O: {
			r: 173,
			g: 216,
			b: 240,
			type: 1
		},
		C: {
			r:50,
			g:20,
			b:30,
			type: 2
		},
		H:{
			r:210,
			g:210,
			b:250,
			type: 3
		},
		Fe:{
			r:220,
			g:105,
			b:30,
			type: 2
		},
		Si:{
			r:90,
			g:120,
			b:90,
			type: 1
		},
		He:{
			r:248,
			g:243,
			b:053,
			type: 3
		},
		Ne:{
			r: 150,
			g: 80,
			b: 80,
			type: 3
		},
		N:{
			r:125, 
			g:33, 
			b:129,
			type: 3
		},
		Mg:{
			r:215,
			g:215,
			b:215,
			type: 1
		},
		S:{
			r:237,
			g:255,
			b:033,
			type: 2
		},
		Ca:{
			r:234,
			g:230,
			b:202,
			type: 1
		},
		Ni:{
			r:240,
			g:240,
			b:240,
			type: 2
		}
	},
	RARE_ELEMENT:{
		Au:{
			r:228, 
			g:158, 
			b:86,
			type: 1
		},
		Cu:{
			r:205, 
			g:127, 
			b:50,
			type: 2
		},
		Ag:{
			r:240,
			g:240,
			b:240,
			type: 1
		},
		Ge:{
			r:140,
			g:140,
			b:140,
			type: 1
		},
		Al:{
			r:250,
			g:250,
			b:250,
			type: 1
		},
		Sn:{
			r:240,
			g:240,
			b:240,
			type: 1
		}
	},
	GALAXY:{
		TYPES: 
		[
			'SBa', 
			'SBb', 
			'Ring'
		],
		SOLAR_SYS_QUANT: 10,
		BLACK_HOLE_QUANT: 5,
		MIN_RADIUS: 20,
		MAX_RADIUS: 110,
		COORDS_LIMIT: 30,
		ROTATION_SPEED: 0.01,
		GALACTIC_OBJ_COORDS_MULTIPLIER: 3
	},
	COLONIZABLE:{
		TYPES:{
			SOLID: 1,
			ROCK: 2,
			GAS: 3
		},
		ATMOSFEAR:{
			NONE: 1,
			NON_BREATHABLE:2,
			BREATHABLE:3
		},
		BASE_COMPOSITION: ["Fe", "Si", "O"],
		MIN_MOON_MASS: 2,
		MAX_MOON_MASS: 5,
		MIN_PLANET_MASS: 25,
		PLANET_SPEED_SCALE: 30,
		MOON_SPEED_SCALE: 15,
	},
	SOLAR_SYSTEM:{
		TYPES:[
			'O',
			'B',
			'A',
			'F',
			'G',
			'K',
			'M',
		],
		SUN:{
			O:{
				radius: 7,
				habitableDist: 1400,
				color: new THREE.Color("rgb(168, 197, 255)")
			},
			B:{
				radius: 4,
				habitableDist: 800,
				color: new THREE.Color("rgb(170, 191, 255)")
			},
			A:{
				radius: 1.6,
				habitableDist: 390,
				color: new THREE.Color("rgb(202, 216, 255)")
			},
			F:{
				radius: 1.2,
				habitableDist: 350,
				color: new THREE.Color("rgb(251, 248, 155)")
			},
			G:{
				radius: 1,
				habitableDist: 310,
				color: new THREE.Color("rgb(235, 201, 160)")
			},
			K:{
				radius: 0.8,
				habitableDist: 270,
				color: new THREE.Color("rgb(235, 201, 160)")
			},
			M:{
				radius: 0.6,
				habitableDist: 230,
				color: new THREE.Color("rgb(255, 169, 91)")
			}
		},
		HABITABLE_DIST_THRESHOLD: 80,
		MIN_DISTANCE: 100,
		MIN_PLANET_NUM: 3,
		MAX_PLANET_NUM: 10
	},
	VIEWS:{
		UNIVERSE: 0,
		GALAXY:1,
		SOLAR_SYSTEM:2,
		PLANET:3
	},
	CAMERA:{
		GALAXY_MOVE_TIME: 2000,
		GALAXY_POS: new THREE.Vector3(0, 300, 600),
		SYSTEM_INIT_POS: new THREE.Vector3(0,1000,1000),
		SYSTEM_POS: new THREE.Vector3(0,300,600),
		UNIVERSE_CONSTRAINTS: [
			{constraint: "userPan", val: true},
			{constraint: "maxPolarAngle", val: 1.22},
			{constraint: "minDistance", val: 500},
			{constraint: "maxDistance", val: 3000},
			{constraint: "panXZOnly", val: true},

		],
		GALAXY_CONSTRAINTS: [
			{constraint: "userPan", val: false},
			{constraint: "maxPolarAngle", val: Math.PI},
			{constraint: "minDistance", val: 300},
			{constraint: "maxDistance", val: 1000},
		],
		SYSTEM_CONSTRAINTS: [
			{constraint: "userPan", val: false},
			{constraint: "maxPolarAngle", val: Math.PI},
			{constraint: "minDistance", val: 100},
			{constraint: "maxDistance", val: 2500},
		],
		PLANET_CONSTRAINTS: [
			{constraint: "userPan", val: true},
			{constraint: "maxPolarAngle", val: Math.PI},
			{constraint: "minDistance", val: 5},
			{constraint: "maxDistance", val: 150},
		],
		NONE_CONSTRAINTS: [
			{constraint: "userPan", val: true},
			{constraint: "maxPolarAngle", val: Math.PI},
			{constraint: "minDistance", val: 0},
			{constraint: "maxDistance", val: 10000},
			{constraint: "panXZOnly", val: false},
		]
	},
	CSS:{
		INVISIBLE_TIME: 500,
		SELECTOR_SCALE: 0.3,
		SELECTOR_ROTATION_SPEED: 0.6,
		GALOBJS_SHOW_DELAY: 300,
		SIDE_MENU_OFFSET_HIDDEN: -350,
		SIDE_MENU_OFFSET_SHOWN: -50,
		HIDE_LOADING_TIME: 2000,
		SHOW_LOADING_TIME: 5000,
		BTN_OPACITY: 0.5,
		BTN_OPACITY_ACTIVE: 0.9,
		MODERATO: 400,
		ALLEGRO: 100,
		ADAGIO: 1000
	},
	ORBIT_SPEED_SCALE: 3,
	PLANET_ROTATION_SPEED: 0.01,
	ORBIT_LINE_COLOR: new THREE.Color("blue"),
	MIN_ZOOM: 400,
	MAX_ZOOM:1000,
	ABSOLUTE_ZERO: 273,
	
};


if(typeof module !== 'undefined' && typeof module.exports !== 'undefined')
	module.exports = Const;

    
  
  