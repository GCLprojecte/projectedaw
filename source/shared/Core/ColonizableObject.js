//////////////////////////////////
///    ColonizableObject       ///
/// ----------------------     ///
/// Objects whose inmediate    ///
/// parent is a SolarSystem    ///
/// ------------------------   ///
//////////////////////////////////

/**
 * @module Core
 */

/**
 * Objects whose inmediate parent is a SolarSystem
 *
 * @class ColonizableObject
 * @constructor
 *
 * @param {int} Identifier
 * @param {String} Name of the moon
 * @param {String[]} Composition of the moon in elements
 * @param {int} Distance
 * @param {int} Radius in million km
 * @param {String} Planet type
 * @param {int} Atmosfear type
 * @param {int} Edificable surface
 * @param {Object} Temperature (min, max)

 */
function ColonizableObject(id, name, composition, distance, radius, type, atmosfear, surface, temperature){
    this.id = id;
    this.owner ="";
    this.name = name;
    this.composition = composition;
    this.distance = distance;
    this.radius = radius;
    this.type = type;
    this.atmosfear = atmosfear;
    this.surface = surface;
    this.temperature =temperature;
}

ColonizableObject.prototype = {

    /**
    * Generates a comma separated list of the composition
    *
    * @method getComposition
    * @return {String} Composition list
    */
    getComposition: function(){
        var composition = "";
        this.composition.forEach(function(element){
            composition += element + ", ";
        });

        composition = composition.substr(0, composition.length-1);

        return composition;
    },


    /**
    * Checks if the planet can be habitable without any technology
    *
    * @method isHabitable
    * @return {Boolean} 
    */
    isHabitable: function(){
        return this.atmosfear == Const.COLONIZABLE.ATMOSFEAR.BREATHABLE && 
            this.type == Const.COLONIZABLE.TYPE.SOLID &&
            this.temperature.min > Const.MIN_HABITABLE_TEMP &&
            this.temperature.max > Const.MAX_HABITABLE_TEMP;
    }
};

if(typeof module !== 'undefined' && typeof module.exports !== 'undefined')
	module.exports = ColonizableObject;