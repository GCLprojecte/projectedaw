/////////////////////////////////////////////////////////
///                     Planet                        ///
/// ------------------------------------------------- ///
/// Objects whose immediate parent is a SolarSystem   ///
/// ------------------------------------------------- ///
/// Extends: ColonizableObject                        ///
/////////////////////////////////////////////////////////

/**
 * @module Core
 */
if(typeof module !== 'undefined' && typeof module.exports !== 'undefined')
	var ColonizableObject = require ('./ColonizableObject.js');

/**
 * Objects whose immediate parent is a SolarSystem
 *
 * @class Planet
 * @constructor
 * @extends ColonizableObject
 *
 * @param {int} Identifier
 * @param {String} Name of the Planet
 * @param {String[]} Composition of the Planet in elements
 * @param {int} Distance to sun
 * @param {int} Radius in million km
 * @param {String} Planet type
 * @param {int} Atmosfear type
 * @param {int} Edificable surface
 * @param {Object} Temperature (min, max)
 * @param {Moon[]} Moons
 *
 */
function Planet(id, name, composition, distance, radius, type, atmosfear, surface, temperature, moons){
    //Initialise super class
    ColonizableObject.call(this, id, name, composition, distance, radius, type, atmosfear, surface, temperature);
    this.moons = moons || [];
}

Planet.prototype = Object.create(ColonizableObject.prototype,{

});

if(typeof module !== 'undefined' && typeof module.exports !== 'undefined')
	module.exports = Planet;