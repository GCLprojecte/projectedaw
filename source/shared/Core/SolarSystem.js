///////////////////////////////
///      SolarSystem        ///
/// ----------------------  ///
/// Extends: GalacticObject ///
/// ----------------------- ///
/// Requires:               ///
/// -GalacticObject.js      ///
///////////////////////////////

/**
*@module Core
*/
if(typeof module !== 'undefined' && typeof module.exports !== 'undefined')
	var GalacticObject = require ('./GalacticObject.js');

/**
* Represents a solar system
*
* @class SolarSystem
* @extends GalacticObject
*/
function SolarSystem(id, name, coords, type, objects){
    //Initialise super class
    GalacticObject.call(this, id, name, coords);

    this.type = type;
    this.planets = objects || [];
}


SolarSystem.prototype = Object.create(GalacticObject.prototype,{

});

if(typeof module !== 'undefined' && typeof module.exports !== 'undefined')
	module.exports = SolarSystem;