/////////////////////////////////////////////////////////
///                     Moon                          ///
/// ------------------------------------------------- ///
/// Objects whose immediate parent is a Planet        ///
/// ------------------------------------------------- ///
/// Extends: ColonizableObject                        ///
/////////////////////////////////////////////////////////

/**
 * @module Core
 */
if(typeof module !== 'undefined' && typeof module.exports !== 'undefined')
	var ColonizableObject = require ('./ColonizableObject.js');

/**
 * Objects whose immediate parent is a Planet
 *
 * @class Moon
 * @constructor
 * @extends ColonizableObject
 *
 * @param {int} Identifier
 * @param {String} Name of the moon
 * @param {String[]} Composition of the moon in elements
 * @param {int} Distance to planet
 * @param {int} Radius in million km
 * @param {String} Planet type
 * @param {int} Atmosfear type
 * @param {int} Edificable surface
 * @param {Object} Temperature (min, max)
 *
 */
function Moon(id, name, composition, distance, radius, type, atmosfear, surface, temperature){
    //Initialise super class
    ColonizableObject.call(this, id, name, composition, distance, radius, type, atmosfear, surface, temperature);
}

Moon.prototype = Object.create(ColonizableObject.prototype,{

});

if(typeof module !== 'undefined' && typeof module.exports !== 'undefined')
	module.exports = Moon;