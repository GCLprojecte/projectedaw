///////////////////////////////////////////////////////
///                 View.Universe                   ///
/// ------------------------------------------------///
/// WebGL Universe generation						///
/// ------------------------------------------------///
///////////////////////////////////////////////////////

/**
*@module View
*/

/**
*@class Universe
*@namespace View
*/
var View = View || {};
(function(){
	"use strict";

		/**
		* Draws the given universe in the scene
		* 
		* @method drawUniverse
		* @param {Universe} Universe
		* @namespace View.Universe
		*/
		View.drawUniverse= function(universe){
			var g = this.webgl.globals,
			uniBase = THREE.Object3D(),
			self = this;

			if(Controller.state.settings.galactic)
				View.conf.settings = Controller.state.settings.galactic;

			universe.galaxies.forEach(function(galaxy){
				
				self.drawGalaxy(galaxy);
			});
		};

		/**
		* Computates a mid point between the galaxies. 
		* Default lookAt point in universe view
		*
		* @method calcGalaxiesMidPoint
		* @param Galaxy[] galaxies
		* @namespace View.Universe
		*/
		View.calcGalaxiesMidPoint= function(galaxies){
			var length = galaxies.length,
			res = new THREE.Vector3();
			for(var i = 0; i < length; i++)
			{
				res.add(new THREE.Vector3(galaxies[i].coords.x, 0, galaxies[i].coords.y));
			}

			res.divideScalar(length);

			return res;
		};
		
		/**
		* Sets the view to the Universe View. 
		* Camera can pan only in the XY plane and 
		* targets the galaxies midpoint.
		*
		* @method setUniverseView
		* @param Universe universe
		* @namespace View.Universe
		*/
		View.setUniverseView= function(universe){
			var target = this.calcGalaxiesMidPoint(universe.galaxies),
			self = this,
			pos = target.clone();
			pos.add(new THREE.Vector3(target.length(),target.length(),0));

			this.setRightMenu(this.generateGalaxyList(universe), function(){
				self.initGalaxyListEvents();
			});

			this.disableControls();
			this.setCameraTarget(target);
			this.setCameraPosition(pos, function(){
				self.setCameraConstraints(Const.CAMERA.UNIVERSE_CONSTRAINTS);
				self.enableControls();
			});
		};


	
})();
