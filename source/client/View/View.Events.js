///////////////////////////////////////////////////////
///                   View.Events                   ///
/// ------------------------------------------------///
/// Event delegation								///
/// ------------------------------------------------///
///////////////////////////////////////////////////////
/**
* @module View
*/

/**
* @class Events
* @namespace View
*/
var View = View || {};
(function(){
	"use strict";

	View.Events = 
	{
		initList:[
			{
				name: "goBackBtn",
				type: "click",
				selector: "#i-btnBack",
				cb: function(){
					Controller.goBack();
				}
			},
			{
				name: "toggleRightMenu",
				type: "click",
				selector: "#i-rightMenu .controller",
				cb: function(){
					View.toggleRightMenu();
				}
			},
			{
				name: "showRightMenu",
				type: "swipeLeft",
				selector: "#i-rightMenu",
				cb: function(){
					View.showRightMenu();
				}
			},
			{
				name: "hideRightMenu",
				type: "swipeRight",
				selector: "#i-rightMenu",
				cb: function(){
					View.hideRightMenu();
				}
			},
			{
				name: "toggleLeftMenu",
				type: "click",
				selector: "#i-leftMenu .controller",
				cb: function(){
					View.toggleLeftMenu();
				}
			},
			{
				name: "showLeftMenu",
				type: "swipeRight",
				selector: "#i-leftMenu",
				cb: function(){
					View.showLeftMenu();
				}
			},
			{
				name: "hideLeftMenu",
				type: "swipeLeft",
				selector: "#i-leftMenu",
				cb: function(){
					View.hideLeftMenu();
				}
			},
			{
				name: "toggleTopMenu",
				type: "click",
				selector: "#i-topMenu .controller",
				cb: function(){
					View.toggleTopMenu();
				}
			},
			{
				name: "showLeftMenu",
				type: "swipeDown",
				selector: "#i-topMenu",
				cb: function(){
					View.showTopMenu();
				}
			},
			{
				name: "hideTopMenu",
				type: "swipeUp",
				selector: "#i-topMenu",
				cb: function(){
					View.hideTopMenu();
				}
			},
			{
				name: "tabsTopMenu",
				type: "click",
				selector: "#i-topMenu .content nav li",
				cb: function(e){
					View.changeTab(e);
				}
			},
			{
				name: "targetSunBtn",
				type: "click",
				selector: "#i-btnSun",
				cb: function(){
					Controller.targetSun();
				}
			},
			{
				name: "targetPlanetBtn",
				type: "click",
				selector: "#i-btnPlanet",
				cb: function(){
					Controller.targetPlanet();
				}
			},
			{
				name: "videoSettingsChange",
				type: "change",
				selector: "#video select, #video input",
				cb: function(){
					Controller.settingsChanged();
				}
			},
			{
				name: "addGalaxyBtn",
				type: "click",
				selector: "#addGalaxy",
				cb: function(){
					Controller.addGalaxy();
				}
			},
			,
			{
				name: "removeUniverse",
				type: "click",
				selector: "#removeUniverse",
				cb: function(){
					Controller.removeUniverse();
				}
			}

		],
		list: [],

		/**
		* @method init
		* @namespace View.Events
		*/
		init: function()
		{
			var self = this;
			this.initList.forEach(function(e){
				self.register(e.name, e.type, e.selector, e.cb);
			});
		},
		/**
		* @method onReady
		* @namespace View.Events
		*/
		onReady: function(cb)
		{
			var loaded = false;
			document.addEventListener("DOMContentLoaded", function(){
				View.Lang.init(cb);
			});
		},
		/**
		* @method register
		* @namespace View.Events
		*/
		register:function(name, eventType, selector, cb){
			if(this.list.indexOf(name) == -1)
			{
				this.list.push({name:name, type:eventType, selector:selector});
				setTimeout(function(){
					$$(selector).on(eventType, cb);
				},10);
			}	
		},
		/**
		* @method unregister
		* @namespace View.Events
		*/
		unregister: function(name){
			this.list.forEach(function(ev){
				if(ev.name == name)
					$$(ev.selector).off(ev.eventType);
			});
		},
		/**
		* @method unregisterAll
		* @namespace View.Events
		*/
		unregisterAll: function(){
			this.list.forEach(function(ev){
				$$(ev.selector).off(ev.eventType);
			});
		}

	};
})();