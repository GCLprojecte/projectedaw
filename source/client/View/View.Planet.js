///////////////////////////////////////////////////////
///                 View.Planet                     ///
/// ------------------------------------------------///
/// WebGL Planet									///
/// ------------------------------------------------///
///////////////////////////////////////////////////////

/**
*@module View
*/

var View = View || {};
(function(){
	"use strict";

	View.setPlanetView = function(planet){
			var self = this;
			var currState = this.hud.state.rightMenu;
			var wgPlanet =this.getWebglPlanet(planet.id);
			var planetMatrix = wgPlanet.children[0].matrixWorld;
			var additiveVector = new THREE.Vector3(planet.radius/3,0,-planet.radius/6);
			var target = new THREE.Vector3().setFromMatrixPosition(planetMatrix).add(additiveVector);
			var gh = this.hud.globals;

			this.hideRightMenu();

			var tween = new TWEEN.Tween(gh.controls.object.position)
			.easing(TWEEN.Easing.Quadratic.InOut)
			.to(target, Const.CAMERA.GALAXY_MOVE_TIME)
			.onUpdate(function(){
				target.setFromMatrixPosition(wgPlanet.children[0].matrixWorld).add(additiveVector);
			})
			.onComplete(function(){
				//View.setCameraTarget(wgPlanet.children[0].position);
				View.hud.globals.controls.object.position = new THREE.Vector3( wgPlanet.children[0].position.x+planet.radius/3,0,-planet.radius/6);
				View.makeCameraChildOf(wgPlanet);
				View.showElement(document.getElementById("i-btnPlanet"));
				View.showElement(document.getElementById("i-btnSun"));
			
			})
			.start();

			//this.openPlanetDialog(planet);

			this.setRightMenu(this.generateMoonsList(planet), function(){
				self.initMoonsListEvents();
				if(currState) self.showRightMenu();
			});

	};

	View.unsetPlanetView = function(planet, system){
		var wgPlanet =this.getWebglPlanet(planet.id);
		var camPos = this.webgl.globals.camera.position;
		var currState = this.hud.state.rightMenu;


		this.hideElement(document.getElementById("i-btnPlanet"));
		this.hideElement(document.getElementById("i-btnSun"));
		this.setCameraConstraints(Const.CAMERA.SYSTEM_CONSTRAINTS);
		this.makeCameraChildOf(this.webgl.globals.scene);
		View.hud.globals.controls.object.position = new THREE.Vector3().setFromMatrixPosition(wgPlanet.children[0].matrixWorld);
		this.setCameraPosition(Const.CAMERA.SYSTEM_POS);
		this.setCameraTarget(new THREE.Vector3());

		this.setRightMenu(this.generatePlanetsList(system), function(){
				View.initPlanetsListEvents();
				if(currState) View.showRightMenu();
		});
	};


	View.getWebglPlanet = function(id){
		var wplanets = View.webgl.globals.planets,
		planets =Controller.getCurrentSolarSystem().planets;
		for(var i = 0; i < planets.length; i++)
			if(planets[i].id == id)
				return wplanets[i];
	};

	View.getPlanetType = function(planet){
		var key;

			switch(planet.type)
			{
				case 1:
					return "rock";
				case 2:
					return "solid";
				case 3:
					return "gas";
			}

	};

	View.getPlanetAtmosfear = function(planet){
		var key;

			switch(planet.type)
			{
				case 1:
				key = "atmosfearNone";
				break;
				case 2:
				key = "nonBreathable";
				break;
				case 3:
				key = "breathable";
				break;
			}

		return View.Lang.literals[key];
	};
	
})();
