///////////////////////////////////////////////////////
///              View.Interface                     ///
/// ------------------------------------------------///
/// Interface functions								///
/// ------------------------------------------------///
///////////////////////////////////////////////////////

/**
*@module View
*/

var View = View || {};
(function(){
	"use strict";

		View.clearInterface = function(cb){
			this.changeTitle("");
			this.hideRightMenu();
			this.hideLeftMenu();
			this.hideElement(document.getElementById("i-btnBack"),cb);
		};
		/**
		* Changes the title.
		*
		* @method changeTitle
		* @param {String} Title
		*/
		View.changeTitle= function(title){
			var hudName = document.getElementById("i-title");

			if(!this.isVisible(hudName))
			{
				hudName.innerHTML = title;
				this.showElement(hudName);
			}
			else
			{
				this.hideElement(hudName,function(){
					hudName.innerHTML = title;
					View.showElement(hudName);
				});
			}
		};

		/**
		* Sets the content of the right menu.
		* If menu is open, hides, updates and shows again
		*
		* @method setRightMenu
		* @param {String} HTML string
		* @param {Function} callback
		*/
		View.setRightMenu = function(html, cb){

			this.hideRightMenu(function(){
				document.querySelector("#i-rightMenu .content").innerHTML = html;
				
				if(cb)cb();
			});
		};
		
		/**
		* Shows or hides the right menu depending on the current state
		*
		* @method toggleRightMenu
		*/
		View.toggleRightMenu = function(){
			if(this.hud.state.rightMenu)
				this.hideRightMenu();
			else
				this.showRightMenu();

		};

		/**
		* Shows the right menu in the case it's hidden
		*
		* @method showRightMenu
		*/
		View.showRightMenu = function(){
			if(this.hud.state.rightMenu) return;
			this.hud.state.rightMenu = true;

			var tween = new TWEEN.Tween({right: Const.CSS.SIDE_MENU_OFFSET_HIDDEN})
			.easing(TWEEN.Easing.Back.Out)
			.to({right:Const.CSS.SIDE_MENU_OFFSET_SHOWN}, Const.CSS.MODERATO)
			.onUpdate(function(){
				document.getElementById("i-rightMenu").style.right = this.right + "px";
			})
			.start();
		};

		/**
		* Hides the left menu in the case is shown
		*
		* @method hideRightMenu
		*/
		View.hideRightMenu = function(cb){
			if(!this.hud.state.rightMenu){
				if(cb)cb();
				return;
			}

			this.hud.state.rightMenu = false;

			var tween = new TWEEN.Tween({right:Const.CSS.SIDE_MENU_OFFSET_SHOWN})
			.easing(TWEEN.Easing.Back.Out)
			.to({right: Const.CSS.SIDE_MENU_OFFSET_HIDDEN}, Const.CSS.MODERATO)
			.onUpdate(function(){
				document.getElementById("i-rightMenu").style.right = this.right + "px";
			})
			.onComplete(function(){
				if(cb)cb();
			})
			.start();
		};

		/**
		* Shows or hides the left menu depending on the current state
		*
		* @method toggleLeftMenu
		*/
		View.toggleLeftMenu = function(){
			if(this.hud.state.leftMenu)
				this.hideLeftMenu();
			else
				this.showLeftMenu();

		};

		/**
		* Shows the left menu in the case it's hidden
		*
		* @method showLeftMenu
		*/
		View.showLeftMenu = function(){
			if(this.hud.state.leftMenu) return;
			this.hud.state.leftMenu = true;

			var tween = new TWEEN.Tween({left: Const.CSS.SIDE_MENU_OFFSET_HIDDEN})
			.easing(TWEEN.Easing.Back.Out)
			.to({left:Const.CSS.SIDE_MENU_OFFSET_SHOWN}, Const.CSS.MODERATO)
			.onUpdate(function(){
				document.getElementById("i-leftMenu").style.left = this.left + "px";
			})
			.start();
		};

		/**
		* Hides the left menu in the case is shown
		*
		* @method hideLeftMenu
		*/
		View.hideLeftMenu = function(){
			if(!this.hud.state.leftMenu) return;
			this.hud.state.leftMenu = false;

			var tween = new TWEEN.Tween({left:Const.CSS.SIDE_MENU_OFFSET_SHOWN})
			.easing(TWEEN.Easing.Back.Out)
			.to({left: Const.CSS.SIDE_MENU_OFFSET_HIDDEN}, Const.CSS.MODERATO)
			.onUpdate(function(){
				document.getElementById("i-leftMenu").style.left = this.left + "px";
			})
			.start();
		};

		/**
		* Shows or hides the top menu depending on the current state
		*
		* @method toggleTopMenu
		*/
		View.toggleTopMenu = function(){
			if(this.hud.state.topMenu)
				this.hideTopMenu();
			else
				this.showTopMenu();

		};

		/**
		* Shows the top menu in the case it's hidden
		*
		* @method showTopMenu
		*/
		View.showTopMenu = function(){
			if(this.hud.state.topMenu) return;
			this.hud.state.topMenu = true;

			var tween = new TWEEN.Tween({top: Const.CSS.SIDE_MENU_OFFSET_HIDDEN})
			.easing(TWEEN.Easing.Back.Out)
			.to({top:Const.CSS.SIDE_MENU_OFFSET_SHOWN}, Const.CSS.MODERATO)
			.onUpdate(function(){
				document.getElementById("i-topMenu").style.top = this.top + "px";
			})
			.start();
		};

		/**
		* Hides the top menu in the case is shown
		*
		* @method hideTopMenu
		*/
		View.hideTopMenu = function(){
			if(!this.hud.state.topMenu) return;
			this.hud.state.topMenu = false;

			var tween = new TWEEN.Tween({top:Const.CSS.SIDE_MENU_OFFSET_SHOWN})
			.easing(TWEEN.Easing.Back.Out)
			.to({top: Const.CSS.SIDE_MENU_OFFSET_HIDDEN}, Const.CSS.MODERATO)
			.onUpdate(function(){
				document.getElementById("i-topMenu").style.top = this.top + "px";
			})
			.start();
		};

		View.setSettings = function(settings){
			document.getElementById("galacticViewResolution").value = settings.galactic.resolution;
			document.getElementById("galacticParticlesQuality").value = settings.galactic.particlesQuality;
			document.getElementById("galacticViewCssTransitions").checked = settings.galactic.transitions;
			document.getElementById("galacticViewAntialiasing").checked = settings.galactic.aa;

			document.getElementById("stellarViewResolution").value = settings.stellar.resolution;
			document.getElementById("stellarViewCssTransitions").checked = settings.stellar.transitions;
			document.getElementById("stellarViewAntialiasing").checked = settings.stellar.aa;

		};

		/**
		* Renders the element. It removes the 'none' class
		*
		* @method renderElement
		* @param DOMElement Element
		*/
		View.renderElement= function(element){
			element.className = element.className.replace(" none", "");
		};

		/**
		* Unrenders the element. It adds the 'none' class
		*
		* @method unrenderElement
		* @param DOMElement Element
		*/
		View.unrenderElement= function(element){
			element.className += " none";
		};

		/**
		* Is the element visible?
		*
		* @method isVisible
		* @param DOMElement element
		* @return boolean
		*/
		View.isVisible= function(element){
			return element.className.indexOf("invisible") == -1;
		};


		/**
		* Standard way of showing an element. It removes the 'invisible' class
		*
		* @method showElement
		* @param DOMElement element
		*/
		View.showElement= function(element){
			this.renderElement(element);
			setTimeout(function(){
				element.className = element.className.replace(" invisible", "");
				
			},10);
		};

		/**
		* Standard way of hiding an element. It adds the 'invisible' class
		*
		* @method hideElement
		* @param DOMElement element
		* @param {Function} callback
		* @param int Time to unrender
		*/
		View.hideElement= function(element, cb, time){
			element.className += " invisible";
			setTimeout(function(){
				View.unrenderElement(element);
				if(cb)cb();
			}, time || Const.CSS.INVISIBLE_TIME);
		};

		View.changeTab = function(e){
			var view = e.target.dataset.pageid;
			if(Controller.state.activeTab == view)
				return;

			Controller.state.activeTab = view;

			var selectedTab = document.querySelector("#i-topMenu .content nav li.selected"),
			activeView = document.querySelector("#i-topMenu .content .page.active");

			selectedTab.className = selectedTab.className.replace("selected", "");
			activeView.className = activeView.className.replace("active", "");

			e.target.className += " selected";
			
			document.getElementById(view).className += " active";

		};

		View.showLoading = function(){
			this.showElement(document.getElementById("loading"));
		};

		View.hideLoading = function(cb){
			this.hideElement(document.getElementById("loading"), cb, Const.CSS.HIDE_LOADING_TIME);
		};

		View.generateSolarSystemList = function(galaxy){
			var ss = galaxy.galacticObjects,
			lang = View.Lang.literals,
			htmlString = "<ul>";
			htmlString += "<li class='title'>" +
			"<h1>"+ galaxy.name + "</h1>"+
			"<div>" + 
				"<p><span>" + lang.coords + "</span>: [ " + galaxy.coords.x + " , " + galaxy.coords.y + " ]</p>"+
				"<p><span>" + lang.radius + "</span>: " + galaxy.radius + " Mpc</p>" +
				"<p><span>" + lang.type + "</span>: "+galaxy.type + "</p>"+
				"<p><span>" + lang.composition +"</span>: " + galaxy.getComposition() + "</p>" +	
			"</div>" +
			"</li>";
			htmlString += "</ul>";
			htmlString += "<ul>";

			ss.forEach(function(system){
				htmlString += "<li data-system='"+system.id+"'>" +
				"<h1>"+ system.name + "</h1>"+
				"<div>" + 
					"<p><span>" + lang.coords + "</span>: [ " + system.coords.x + " , " + system.coords.y + " ]</p>"+
					"<p><span>" + lang.starType + "</span>: " + system.type + "</p>" +
					"<p><span>" + lang.planets + "</span>: "+system.planets.length + "</p>"+
				"</div>" +
				"</li>";
			});

			htmlString += "</ul>";

			return htmlString;
		};

		View.initSolarSystemListEvents = function(){
			View.Events.register("liSystemHover", "mouseenter", "#i-rightMenu .content ul li[data-system]", Controller.systemOnHover);
			View.Events.register("liSystemMouseOut", "mouseleave", "#i-rightMenu .content ul li[data-system]", Controller.systemOnMouseOut);
			View.Events.register("liSystemClick", "click", "#i-rightMenu .content ul li[data-system]", Controller.systemOnClick);
			View.Events.register("liSystemTap", "tap", "#i-rightMenu .content ul li[data-system]", Controller.systemOnClick);
		};

		View.generateGalaxyList = function(universe){
			var galaxies = universe.galaxies,
			lang = View.Lang.literals,
			htmlString = "<ul>",
			composition = [];

			galaxies.forEach(function(glx){
				composition = glx.getComposition();

				htmlString += "<li data-galaxy='"+glx.id+"'>" +
				"<h1>"+ glx.name + "</h1>"+
				"<div>"+
					"<p><span>" + lang.coords + "</span>: [ " + glx.coords.x + " , " + glx.coords.y + " ]</p>"+
					"<p><span>" + lang.radius + "</span>: " + glx.radius + " Mpc</p>" +
					"<p><span>" + lang.type +"</span>: " + glx.type + "</p>" +
				"</div>" +
				"</li>";
			});

			htmlString += "</ul>";

			return htmlString;
		};

		View.initGalaxyListEvents = function(){
			View.Events.register("liGalaxyHover", "mouseenter", "#i-rightMenu .content ul li[data-galaxy]", Controller.galaxyOnHover);
			View.Events.register("liGalaxyMouseOut", "mouseleave", "#i-rightMenu .content ul li[data-galaxy]", Controller.galaxyOnMouseOut);
			View.Events.register("liGalaxyClick", "click", "#i-rightMenu .content ul li[data-galaxy]", Controller.galaxyOnClick);
			View.Events.register("liGalaxyTap", "tap", "#i-rightMenu .content ul li[data-galaxy]", Controller.galaxyOnClick);
		};

		View.generatePlanetsList = function(system){
			var planets = system.planets,
			lang = View.Lang.literals,
			info = Const.SOLAR_SYSTEM.SUN[system.type],
			htmlString = "<ul>";

			htmlString += "<li class='title'>" +
				"<h1>"+ system.name + "</h1>"+
				"<div>" + 
					"<p><span>" + lang.coords + "</span>: [ " + system.coords.x + " , " + system.coords.y + " ]</p>"+
					"<p><span>" + lang.planets + "</span>: "+system.planets.length + "</p>"+
					"<p><span>" + lang.starType + "</span>: " + system.type + "</p>" +
					"<p><span>" + lang.starRadius + "</span>: ~" + (info.radius*7).toFixed(2) + " x 10⁵ Km</p>" +
					"<p><span>" + lang.habitableZone + "</span>: [ " + 
						(info.habitableDist - Const.SOLAR_SYSTEM.HABITABLE_DIST_THRESHOLD) + " , " +
						(info.habitableDist + Const.SOLAR_SYSTEM.HABITABLE_DIST_THRESHOLD) + " ]" +
					"</p>" +	
				"</div>" +
				"</li>";
			
			htmlString += "</ul><ul>";
			
			planets.forEach(function(planet){
				htmlString += "<li data-planet='"+planet.id+"'>" + 
				"<h1>"+ planet.name + "</h1>"+
				"<div>"+
					"<p><span>" + lang.distance + "</span>: "+ (planet.distance/10).toFixed(2) +" x 10⁶ Km</p>" +
					"<p><span>" + lang.radius +"</span>: " + planet.radius.toFixed(2) + " x 10² Km</p>";
					if(planet.moons.length > 0)
						htmlString +="<p><span>" + lang.moons +"</span>: " + planet.moons.length+"</p>";

				htmlString+=
				"</div>" +
				"</li>";
			});

			htmlString += "</ul>";

			return htmlString;
		};
	
		View.initPlanetsListEvents = function(){
			View.Events.register("liPlanetHover", "mouseenter", "#i-rightMenu .content ul li[data-planet]", Controller.planetOnHover);
			View.Events.register("liPlanetMouseOut", "mouseleave", "#i-rightMenu .content ul li[data-planet]", Controller.planetOnMouseOut);
			View.Events.register("liPlanetClick", "click", "#i-rightMenu .content ul li[data-planet]", Controller.planetOnClick);
			View.Events.register("liPlanetTap", "tap", "#i-rightMenu .content ul li[data-planet]", Controller.planetOnClick);
		};

		View.generateMoonsList = function(planet){
			var moons = planet.moons,
			lang = View.Lang.literals,
			htmlString = "<ul>";

			htmlString += "<li class='title'>" +
				"<h1>"+ planet.name + "</h1>"+
				"<div>" + 
					"<p><span>" + lang.distance + "</span>: "+ (planet.distance/10).toFixed(2) +" x 10⁶ Km</p>" +
					"<p><span>" + lang.radius +"</span>: " + planet.radius.toFixed(2) + " x 10² Km</p>" +
					"<p><span>" + lang.type + "</span>:" + lang[View.getPlanetType(planet)] + "</p>" +
					"<p><span>" + lang.surface + "</span>:" + planet.surface.toFixed(2) + " Km²" +"</p>"+
					"<p><span>" + lang.composition + "</span>:" + planet.getComposition()+	"</p>"+
					"<p><span>" + lang.atmosfear + "</span>:" + this.getPlanetAtmosfear(planet) + "</p>" +
					"<p><span>" + lang.temperature + "</span>:" +planet.temperature.min.toFixed(2) + " / "+planet.temperature.max.toFixed(2) + " Cº</p>" +	
				"</div>" +
				"</li>";
			
			htmlString += "</ul><ul>";
			
			moons.forEach(function(moon){
				htmlString += "<li data-moon='"+moon.id+"'>" + 
				"<h1>"+ moon.name + "</h1>"+
				"<div>"+
					"<p><span>" + lang.distance + "</span>: "+ (moon.distance/10).toFixed(2) +" x 10⁶ Km</p>" +
					"<p><span>" + lang.radius +"</span>: " + moon.radius.toFixed(2) + " x 10² Km</p>" +
				"</div>" +
				"</li>";
			});

			htmlString += "</ul>";

			return htmlString;
		};

		View.initMoonsListEvents = function(){
			View.Events.register("liMoonHover", "mouseenter", "#i-rightMenu .content ul li[data-moon]", Controller.moonOnHover);
			View.Events.register("liMoonMouseOut", "mouseleave", "#i-rightMenu .content ul li[data-moon]", Controller.moonOnMouseOut);
			View.Events.register("liMoonClick", "click", "#i-rightMenu .content ul li[data-moon]", Controller.moonOnClick);
			View.Events.register("liMoonTap", "tap", "#i-rightMenu .content ul li[data-moon]", Controller.moonOnClick);
		};
		View.setMoonDialog = function(moon){
			var lang = View.Lang.literals;
			if(this.hud.state.moonDialog){
				this.hud.state.moonDialogLast = true;
				this.hideElement(document.getElementById("i-moonDialog"), function(){
					View.hud.state.moonDialog = false;
					View.setMoonDialog(moon);
				});
			}
			else
			{
				document.getElementById("i-moonName").textContent =moon.name;
				document.getElementById("i-moonDistance").textContent =moon.distance.toFixed(2) + " x 10⁶ Km";
				document.getElementById("i-moonRadius").textContent =moon.radius.toFixed(2) + " x 10² Km";
				document.getElementById("i-moonType").textContent =lang[this.getPlanetType(moon)];
				document.getElementById("i-moonSurface").textContent =moon.surface.toFixed(2) + " Km²";
				document.getElementById("i-moonComposition").textContent =moon.getComposition() + " Km²";
				document.getElementById("i-moonAtmosfear").textContent =this.getPlanetAtmosfear(moon);
				document.getElementById("i-moonTemperature").textContent =moon.temperature.min.toFixed(2) + " / "+moon.temperature.max.toFixed(2) + " Cº";
				if(this.hud.state.moonDialogLast)
				{
					this.hud.state.moonDialogLast = false;
					this.showElement(document.getElementById("i-moonDialog"));
				}
				
			}
		};

		View.openMoonDialog = function(moon){
			this.setMoonDialog(moon);
			this.showElement(document.getElementById("i-moonDialog"));
			this.hud.state.moonDialog = true;
		};
})();
