///////////////////////////////////////////////////////
///              View.SolarSystem                   ///
/// ------------------------------------------------///
/// WebGL SolarSystem generation					///
/// ------------------------------------------------///
///////////////////////////////////////////////////////

/**
*@module View
*/

/**
* @class SolarSystem
* @namespace View
*/

var View = View || {};
(function(){
	"use strict";


	/**
	* @method setSolarSystemView
	* @namespace View.SolarSystem
	*/
	View.setSolarSystemView = function(system, galaxyCoords){
		var pos = new THREE.Vector3(
			galaxyCoords.x + system.coords.x * Const.GALAXY.GALACTIC_OBJ_COORDS_MULTIPLIER,
			0, 
			galaxyCoords.y + system.coords.y * Const.GALAXY.GALACTIC_OBJ_COORDS_MULTIPLIER
		),
		self = this,
		currState = this.hud.state.rightMenu;

		this.disableControls();
		this.setCameraTarget(pos);
		this.setCameraPosition(pos, function(){}, TWEEN.Easing.Quintic.In);
		

		setTimeout(function(){
			self.showLoading();
		}, 200);

		setTimeout(function(){
			if(Controller.state.settings.stellar)
				View.conf.settings = Controller.state.settings.stellar;
			self.reset(function(){
				self.hud.globals.controls.object.position = Const.CAMERA.SYSTEM_INIT_POS;
				self.drawSolarSystem(system);
				self.setCameraConstraints(Const.CAMERA.SYSTEM_CONSTRAINTS);
				self.setCameraPosition(Const.CAMERA.SYSTEM_POS, function(){}, TWEEN.Easing.Quintic.Out);
				self.hideLoading();
				self.setRightMenu(self.generatePlanetsList(system), function(){
					self.initPlanetsListEvents();
					self.changeTitle(system.name);
					self.showElement(document.getElementById("i-btnBack"));

					if(currState) self.showRightMenu();

				});

			});
		},Const.CSS.SHOW_LOADING_TIME);

	};
	/**
	* @method unsetSolarSystemView
	* @namespace View.SolarSystem
	*/
	View.unsetSolarSystemView = function(system, galaxy){
		var pos = Const.CAMERA.SYSTEM_INIT_POS,
		self = this,
		currState = this.hud.state.rightMenu;

		 

		this.disableControls();
		this.setCameraPosition(pos, function(){}, TWEEN.Easing.Quintic.In);
		setTimeout(function(){
			self.showLoading();
		}, 200);

		setTimeout(function(){
			if(Controller.state.settings.galactic)
				View.conf.settings = Controller.state.settings.galactic;
			self.reset(function(){
				self.hud.globals.controls.object.position = new THREE.Vector3(
					galaxy.coords.x + system.coords.x * Const.GALAXY.GALACTIC_OBJ_COORDS_MULTIPLIER,
					0, 
					galaxy.coords.y + system.coords.y * Const.GALAXY.GALACTIC_OBJ_COORDS_MULTIPLIER
				);
				self.drawUniverse(Controller.state.universe);
				setTimeout(function(){
					var galaxyHuds = document.querySelectorAll(".glx-name, .galaxy"),
					galxpos = new THREE.Vector3(galaxy.coords.x, 0, galaxy.coords.y),
					camPos = (new THREE.Vector3()).copy(galxpos).add(Const.CAMERA.GALAXY_POS);

					//Transitions
					for(var i = 0; i < galaxyHuds.length; i++){
						self.hideElement(galaxyHuds[i]);
					}
					self.hud.globals.controls.center = galxpos;
					self.setCameraPosition(camPos, function(){}, TWEEN.Easing.Quintic.Out);
					self.hideLoading();
					self.showElement(document.getElementById("i-btnBack"));
					self.setRightMenu(self.generateSolarSystemList(galaxy), function(){
						self.initSolarSystemListEvents();
					});
					setTimeout(function(){			
						//Show name in global hud
						self.changeTitle(galaxy.name);

						self.showGalacticObjects(galaxy.id);
						self.setCameraConstraints(Const.CAMERA.GALAXY_CONSTRAINTS);
						self.enableControls();

						if(currState) self.showRightMenu();

					}, Const.CSS.INVISIBLE_TIME);
				},10);
			});
		},Const.CSS.SHOW_LOADING_TIME);
	};

	/**
	* Draws a solarSystem
	* 
	* @class drawSolarSystem 
	* @namespace View.SolarSystem
	*/
	View.drawSolarSystem = function(SolarSystem){
		var requiredTextures = {
			sunflare: "/assets/textures/lensFlare/sunLensFlare2.png",
			halo: "/assets/textures/lensFlare/sun_halo.png",
			sunsurface: "/assets/textures/planet/sun_surface.png",
			sskydome: "/assets/textures/skybox/solarSystem.jpg",
			gas: "/assets/textures/planet/gas.png",
			solid: "/assets/textures/planet/solid.png",
			rock: "/assets/textures/planet/rock.jpg",
			b_gas: "/assets/textures/planet/bump/gas.png",
			b_solid: "/assets/textures/planet/bump/solid.png",
			b_rock: "/assets/textures/planet/bump/rock.jpg",
			s_gas: "/assets/textures/planet/specular/gas.png",
			s_solid: "/assets/textures/planet/specular/solid.png",
			s_rock: "/assets/textures/planet/specular/rock.jpg",
			
		};
		this.loadTextures(requiredTextures, function(){
			draw(SolarSystem);
		});
	};

	function draw(SolarSystem){
		drawSun(SolarSystem.type);
		drawPlanets(SolarSystem);
		drawOrbits(SolarSystem);
		drawSkydome();
		drawHud(SolarSystem);
		initUpdates();
	}

	function drawSkydome(){
		var t = View.webgl.textures,
		gw = View.webgl.globals,
		skyBox;

		var geometry = new THREE.SphereGeometry(8000, 60, 40);
		var uniforms = {
			texture: { type: 't', value: t.sskydome}
		};

		var material = new THREE.ShaderMaterial( {
			uniforms: uniforms,
			vertexShader: document.getElementById('sky-vertex').textContent,
			fragmentShader: document.getElementById('sky-fragment').textContent
		});

		skyBox = new THREE.Mesh(geometry, material);
		skyBox.scale.set(-1, 1, 1);
		skyBox.eulerOrder = 'XZY';
		skyBox.renderDepth = 1000.0;
		gw.scene.add(skyBox);
	
	}

	function drawHud(SolarSystem){
		var info = Const.SOLAR_SYSTEM.SUN[SolarSystem.type],
		gh = View.hud.globals,
		planets = SolarSystem.planets;

		planets.forEach(function(planet){
			var distance = planet.distance + info.radius,
			center = new THREE.Object3D(),
			planetPosition = new THREE.Object3D(),
			basePlanet = document.createElement("div");

			basePlanet.className = "planet";
			basePlanet.dataset.planet = planet.id;
			basePlanet.style.width=(planet.radius/2)+"px";
			basePlanet.style.height=(planet.radius/2)+"px";
			basePlanet.style.borderRadius = (planet.radius / 4) + "px";

			var hudPlanet = new THREE.CSS3DObject(basePlanet);
			planetPosition.position.x = distance;
			hudPlanet.rotation.x = Util.degToRad(90);

			planetPosition.add(hudPlanet);
/*
			planet.moons.forEach(function(moon){
				var mdistance = moon.distance + planet.radius,
				centerPlanet = new THREE.Object3D(),
				baseMoon = document.createElement("div");

				var moonHud = new THREE.CSS3DObject(baseMoon);

				moonHud.scale.x = moon.radius/10;
				moonHud.scale.y = moon.radius/10;
				moonHud.scale.z = moon.radius/10;
				moonHud.position.x = mdistance;

				centerPlanet.add(moonHud);
				planetPosition.add(centerPlanet);

				centerPlanet.rotation.y = Date.now() * Const.COLONIZABLE.MOON_SPEED_SCALE/mdistance;
			});
*/
			center.add(planetPosition);
			center.rotation.y = Date.now() * Const.COLONIZABLE.PLANET_SPEED_SCALE/distance;

			gh.planets.push(center);
			gh.scene.add(center);

		});

	}

	function drawPlanets(SolarSystem){
		var info = Const.SOLAR_SYSTEM.SUN[SolarSystem.type],
		geo = View.webgl.geometries,
		gw = View.webgl.globals,
		gv = View.globals,
		t = View.webgl.textures,
		planets = SolarSystem.planets;

		if(!geo.gassphere) geo.gassphere = new THREE.SphereGeometry(1,32,32);
		if(!geo.rocksphere) geo.rocksphere = new THREE.SphereGeometry(1,32,32);
		if(!geo.solidsphere) geo.solidsphere = new THREE.SphereGeometry(1,32,32);
		if(!geo.moonsphere) geo.moonsphere = new THREE.SphereGeometry(1,32,32);

		if(gw.planets) gw.planets = [];

		planets.forEach(function(planet){
			var distance = planet.distance + info.radius,
			type = View.getPlanetType(planet),
			material = new THREE.MeshPhongMaterial({
				color: View.getCompositionColor(planet.composition), 
				map:t[type],
				specularMap:t["s_"+type],
				bumpMap:t["b_"+type],
				bumpScale:0.6
			}),
			planetPosition = new THREE.Object3D(),
			sphere = geo[View.getPlanetType(planet)+"sphere"],
			planetMesh = new THREE.Mesh(sphere, material),
			sunCenter = new THREE.Object3D();

			planetMesh.scale.x = planet.radius/10;
			planetMesh.scale.y = planet.radius/10;
			planetMesh.scale.z = planet.radius/10;
			planetMesh.rotation.y = Date.now() * Const.PLANET_ROTATION_SPEED * planet.radius/10;
			planetPosition.add(planetMesh);
			planetPosition.position.x = distance;

			planetMesh.matrixAutoUpdate = true;
			
			planet.moons.forEach(function(moon){
				var mdistance = moon.distance*10 + planet.radius/100,
				type = View.getPlanetType(planet),
				material = new THREE.MeshPhongMaterial({
					color: View.getCompositionColor(moon.composition), 
					map:t[type],
					specularMap:t["s_"+type],
					bumpMap:t["b_"+type],
					bumpScale:0.6
				}),
				sphere = geo[View.getPlanetType(moon)+"sphere"],
				moonMesh = new THREE.Mesh(sphere, material),
				pcenter = new THREE.Object3D();



				moonMesh.scale.x = moon.radius/2;
				moonMesh.scale.y = moon.radius/2;
				moonMesh.scale.z = moon.radius/2;
				
				moonMesh.position.x = mdistance;

				pcenter.add(moonMesh);
				planetPosition.add(pcenter);

				pcenter.rotation.y = Date.now() * Const.COLONIZABLE.MOON_SPEED_SCALE/mdistance;

			});
			sunCenter.add(planetPosition);
			sunCenter.rotation.y = Date.now() * Const.COLONIZABLE.PLANET_SPEED_SCALE/distance;

			gw.planets.push(sunCenter);
			gw.scene.add(sunCenter);

			
		});
	}

	function initUpdates(globalIndex){
		var gv = View.globals,
		gw = View.webgl.globals,
		gh = View.hud.globals,
		c = View.conf,
		globalCamPos = new THREE.Vector3();

		gv.updateFunctions.push(function(delta){
			gw.halo.lookAt(globalCamPos.setFromMatrixPosition(View.webgl.globals.camera.matrixWorld));
			gw.sun.rotation.y += delta * Const.COLONIZABLE.PLANET_SPEED_SCALE/450;
			gw.planets.forEach(function(planet){
				planet.rotation.y += delta * Const.COLONIZABLE.PLANET_SPEED_SCALE/planet.children[0].position.x;
				planet.children[0].children[0].rotation.y += Const.PLANET_ROTATION_SPEED * planet.scale.x/10;

				for(var i= 1; i< planet.children[0].children.length; i++)
				{
					planet.children[0].children[i].rotation.y += delta * Const.COLONIZABLE.MOON_SPEED_SCALE/planet.children[0].children[i].children[0].position.x;
				}

			});
			gh.planets.forEach(function(planet, i){
				planet.rotation.y = gw.planets[i].rotation.y;
			});
			if(c.showOrbit){
				gw.orbits.forEach(function(orbit, i){
					if(orbit.material.opacity < 1)
						orbit.material.opacity += delta * 0.5 * (i*0.20+1);
				});
			}
			else
			{
				gw.orbits.forEach(function(orbit, i){
					if(orbit.material.opacity > 0)
						orbit.material.opacity -= delta * 0.5 * (i*0.20+1);
				});
			}
				
		});
	}


	function drawSun(type){
		var info = Const.SOLAR_SYSTEM.SUN[type],
		geo = View.webgl.geometries,
		gw = View.webgl.globals,
		t = View.webgl.textures;
		//Init
		if(!geo.sunsphere) geo.sunsphere = new THREE.SphereGeometry(1,32,32);
		var halo = new THREE.Gyroscope();
		var haloMaterial = new THREE.MeshBasicMaterial({
			map: t.halo,
			transparent:true,
			blending:THREE.AdditiveBlending,
			depthWrite: false,
			color: info.color
		}),
		plane = new THREE.PlaneGeometry(info.radius*65, info.radius*65),
		haloMesh= new THREE.Mesh(plane, haloMaterial);
		var flareMaterial = new THREE.MeshBasicMaterial({
			map: t.sunflare,
			transparent:true,
			blending:THREE.AdditiveBlending,
			depthWrite: false,
			color: info.color
		}),
		flareplane = new THREE.PlaneGeometry(info.radius*150, info.radius*150),
		flareMesh= new THREE.Mesh(flareplane, flareMaterial);

		//Set
		halo.add(haloMesh);
		halo.add(flareMesh);
		flareMesh.position.z += info.radius*22;

		var light = new THREE.PointLight(0xFFFFFF, 1, 2000);
		var ambientLight = new THREE.AmbientLight(0x222222);
		var material = new THREE.MeshBasicMaterial({color: info.color, map: t.sunsurface}),
		sphereGeo = geo.sunsphere,
		mesh = new THREE.Mesh(sphereGeo, material);
		
		
		gw.halo = halo;
		gw.sun = mesh;
		mesh.scale.x = info.radius*20;
		mesh.scale.y = info.radius*20;
		mesh.scale.z = info.radius*20;

		//Draw
		gw.scene.add(halo);
		gw.scene.add(ambientLight);

		gw.scene.add(light);
		gw.scene.add(mesh);
	}


	function drawOrbits(SolarSystem){
		var geo = View.webgl.geometries,
		gw = View.webgl.globals,
		gv = View.globals,
		c = View.conf;

		if(!geo.circle){
			geo.circle = new THREE.CircleGeometry(1,128);
			//remove center vertex
			geo.circle.vertices.shift();
		} 

		var info = Const.SOLAR_SYSTEM.SUN[SolarSystem.type],
		circleGeo = geo.circle,
		planets = SolarSystem.planets;

		if(gw.orbits) gw.orbits = [];

		planets.forEach(function(planet, i){
			var distance = planet.distance + info.radius,
			material = new THREE.LineBasicMaterial({
				color: Const.ORBIT_LINE_COLOR,
				opacity: 1, 
				blending: THREE.AdditiveBlending, 
				transparent: true,
				side: THREE.DoubleSide
			}),

			line = new THREE.Line(circleGeo, material);

			line.scale.x = distance;
			line.scale.y = distance;
			line.rotation.x = Util.degToRad(90);

			gw.scene.add(line);
			gw.orbits.push(line);

		});
	}

})();
