///////////////////////////////////////////////////////
///                   View.Galaxy                   ///
/// ------------------------------------------------///
/// WebGL galaxy generation							///
/// ------------------------------------------------///
///////////////////////////////////////////////////////

/**
*@module View
*/

	/**
	* @class Galaxy
	* @namespace View
	*/

var View = View || {};
(function(){
	"use strict";

	/**
	* Sets the view to the Galaxy View. 
	* Camera centers to the target galaxy and shows its solar systems
	*
	* @method setGalaxyView
	* @param Galaxy target
	* @namespace Galaxy
	*/
	View.setGalaxyView =  function(galaxy){
		var pos = new THREE.Vector3(galaxy.coords.x, 0, galaxy.coords.y),
		self = this,
		camPos = (new THREE.Vector3()).copy(pos).add(Const.CAMERA.GALAXY_POS),
		galaxyHuds = document.querySelectorAll(".glx-name, .galaxy"),
		currState = this.hud.state.rightMenu;

		this.disableControls();
		this.setCameraTarget(pos);
		this.setCameraPosition(camPos, function(){

			self.setCameraConstraints(Const.CAMERA.GALAXY_CONSTRAINTS);
			self.enableControls();

			if(currState) self.showRightMenu();
		});

		this.showElement(document.getElementById("i-btnBack"));
		this.setRightMenu(this.generateSolarSystemList(galaxy), function(){
			self.initSolarSystemListEvents();
		});

		//Transition
		for(var i = 0; i < galaxyHuds.length; i++){
			this.hideElement(galaxyHuds[i]);
		}

		setTimeout(function(){			
			//Show name in global hud
			self.changeTitle(galaxy.name);

			self.showGalacticObjects(galaxy.id);
		}, Const.CSS.INVISIBLE_TIME);

	};

	/**
	* Unsets the galaxy view. This is needed if the galaxy view has been set.
	* Hides galactic objects, hides the galaxy name and shows the galaxy selector controls
	*
	* @method unsetGalaxyView
	* @param galaxy
	* @namespace Galaxy
	*/
	View.unsetGalaxyView = function(galaxy){
		var galaxyHuds = document.querySelectorAll(".glx-name, .galaxy");

		View.hideGalacticObjects(galaxy.id);
		this.hideElement(document.getElementById("i-title"));
		this.hideElement(document.getElementById("i-btnBack"));

		for(var i = 0; i < galaxyHuds.length; i++)
		{
			this.showElement(galaxyHuds[i]);
			this.renderElement(galaxyHuds[i]);
			
		}

	};

	/**
	* Hides the galactics objects of a galaxy
	*
	* @method hideGalacticObjects
	* @param {String} Galaxy ID
	* @namespace Galaxy
	*/
	View.hideGalacticObjects = function(id){
		var gobjs = document.querySelectorAll(".galacticObject[data-galaxy="+id+"]");

		for(var i= 0; i < gobjs.length; i++)
		{
			this.hideElement(gobjs[i]);
		}
	};

	/**
	* Shows galactic objects one by one.
	*
	* @param {String} Id Galaxy id
	* @namespace Galaxy
	* 
	*/
	View.showGalacticObjects = function(id){
		var gobjs = document.querySelectorAll(".galacticObject[data-galaxy="+id+"]"),
		self = this,
		i = 0;
		
		(function recurShow(){
			if(i < gobjs.length)
			{
				self.showElement(gobjs[i]);
				i++;
				setTimeout(recurShow, Const.CSS.GALOBJS_SHOW_DELAY);	
			}
		})();
	};

	/**
	* Draws a galaxy
	* 
	* @class drawGalaxy 
	* @namespace Galaxy
	*/
	View.drawGalaxy = function(galaxy){

		var requiredTextures = {};

		requiredTextures = {
			galaxyLensFlare: 'assets/textures/lensFlare/GalaxyLensFlare.png',
			particle: 'assets/textures/galaxy/particle.png',
			gskydome: 'assets/textures/skybox/galaxies.jpg'
		};
		requiredTextures["galaxyBase"+galaxy.type] = 'assets/textures/galaxy/galaxia'+galaxy.type+'.png';
		

		this.loadTextures(
			requiredTextures,
			function(){
				draw(galaxy);
			}
		);
	};

	function draw(galaxy){
		var t = View.webgl.textures,
		gw = View.webgl.globals,
		gv = View.globals,
		globalIndex = gw.galaxies.length,
		compositionColor = View.getCompositionColor(galaxy.composition),
		gRadius = galaxy.radius*10,
		gPosition = new THREE.Vector3(galaxy.coords.x, 0, galaxy.coords.y),
		lensFlare = new THREE.LensFlare(
				t.galaxyLensFlare,
				Math.min(gRadius*2,1000) * View.conf.settings.resolution,
				0.0,
				THREE.AdditiveBlending,
				new THREE.Color("white")
		),
		galaxyBaseMaterial = new THREE.MeshBasicMaterial({
			map: t["galaxyBase"+galaxy.type],
			transparent: true,
			depthWrite: false,
			side: THREE.DoubleSide,
			blending: THREE.AdditiveBlending,
			color: compositionColor
			//wireframe:true
		}),
		plane = new THREE.PlaneGeometry(gRadius, gRadius),
		galaxyBase = new THREE.Mesh(plane, galaxyBaseMaterial),
		particles = initParticles(galaxy);
		
		galaxyBase.overdraw = true;
		galaxyBase.add(lensFlare);

		
		galaxyBase.rotation.x = Util.degToRad(90);
		particles.rotation.x -= Util.degToRad(90);
		galaxyBase.add(particles);
		
		galaxyBase.position = gPosition;
		galaxyBase.unid = galaxy.id;

		gw.galaxies.push(galaxyBase);
		gw.scene.add(galaxyBase);

		drawHud(galaxy);
		drawSkybox();

		initEvents();
		initUpdates(globalIndex);
	}

	function drawSkybox(){
		var t = View.webgl.textures,
		gw = View.webgl.globals,
		skyBox;

		var geometry = new THREE.SphereGeometry(5000, 60, 40);
		var uniforms = {
			texture: { type: 't', value: t.gskydome}
		};

		var material = new THREE.ShaderMaterial( {
			uniforms: uniforms,
			vertexShader: document.getElementById('sky-vertex').textContent,
			fragmentShader: document.getElementById('sky-fragment').textContent
		});

		skyBox = new THREE.Mesh(geometry, material);
		skyBox.scale.set(-1, 1, 1);
		skyBox.eulerOrder = 'XZY';
		skyBox.renderDepth = 1000.0;
		gw.scene.add(skyBox);
	}

	function drawHud(galaxy){
		var t = View.webgl.textures,
		gh = View.hud.globals,
		gv = View.globals,
		gw = View.webgl.gobals,
		gRadius = galaxy.radius*10,
		gPosition = new THREE.Vector3(galaxy.coords.x, 0, galaxy.coords.y), //(x, z, y)
		hudBase = document.createElement('div'),
		galacticObjects = drawGalacticObjects(galaxy.galacticObjects, galaxy.id),
		hudName = document.createElement('div');
		
		hudName.innerHTML = "<span>"+galaxy.name+"</span>";
		hudName.className = 'glx-name';
		hudBase.className = 'galaxy';

		hudBase.dataset.galaxy = galaxy.id;
		hudName.dataset.galaxy = galaxy.id;

		hudBase.style.height = gRadius + "px";
		hudBase.style.width = gRadius + "px";
		hudBase.style.borderRadius = (gRadius / 2) + "px";


		var hudGalaxy = new THREE.CSS3DObject(hudBase);
		hudGalaxy.add(galacticObjects);
		hudGalaxy.position= gPosition;

		var hudName3d = new THREE.CSS3DObject(hudName);
		hudName3d.position= gPosition;

		gh.galaxyNames.push(hudName3d);
		gh.scene.add(hudName3d);
		gh.galaxies.push(hudGalaxy);
		gh.scene.add(hudGalaxy);

		hudGalaxy.rotation.x -= Util.degToRad(90);
		
	}

	function initEvents(){
		View.Events.register("galaxyOnHover", "mouseover", ".galaxy", Controller.galaxyOnHover);
		View.Events.register("galaxyOnMouseOut", "mouseout", ".galaxy", Controller.galaxyOnMouseOut);
		View.Events.register("galaxyOnClick", "click", ".galaxy", Controller.galaxyOnClick);
		View.Events.register("galaxyOnTap", "tap", ".galaxy", Controller.galaxyOnClick);

		View.Events.register("systemOnHover", "mouseover", ".galacticObject", Controller.systemOnHover);
		View.Events.register("systemOnnMouseOut", "mouseout", ".galacticObject", Controller.systemOnMouseOut);
		View.Events.register("systemOnClick", "click", ".galacticObject", Controller.systemOnClick);
		View.Events.register("systemOnTap", "tap", ".galactiecObject", Controller.systemOnClick);
	}

	function initUpdates(globalIndex){
		var gh = View.hud.globals,
		gw = View.webgl.globals,
		gv = View.globals;

		gv.updateFunctions.push(function(delta){
			gh.galaxyNames[globalIndex].lookAt(gh.camera.position);
			gw.galaxies[globalIndex].rotation.z -= Const.GALAXY.ROTATION_SPEED*delta;
			gh.galaxies[globalIndex].rotation.copy(gw.galaxies[globalIndex].rotation);
			gh.galaxies[globalIndex].children[0].children.forEach(function(selector){
				selector.rotation.z += Const.CSS.SELECTOR_ROTATION_SPEED*delta;
			});
		});

	}

	function drawGalacticObjects(gobjs, id){
		var gh = View.hud.globals,
		gv = View.globals,
		center = new THREE.Object3D();

		gobjs.forEach(function(go){
			var goDom = document.createElement('div');
			goDom.className = " invisible galacticObject";
			goDom.dataset.galaxy = id;
			goDom.dataset.system = go.id;

			var goMesh = new THREE.CSS3DObject(goDom);
			goMesh.position = new THREE.Vector3(
				go.coords.x*Const.GALAXY.GALACTIC_OBJ_COORDS_MULTIPLIER, 
				go.coords.y*Const.GALAXY.GALACTIC_OBJ_COORDS_MULTIPLIER, 
				0
			);
			goMesh.scale.x = Const.CSS.SELECTOR_SCALE;
			goMesh.scale.y = Const.CSS.SELECTOR_SCALE;
			goMesh.scale.z = Const.CSS.SELECTOR_SCALE;

			center.add(goMesh);

		});
		return center;
		
	}

	function initParticles(galaxy){
		var t = View.webgl.textures,
		compositionColor = View.getCompositionColor(galaxy.composition),
		gRadius = galaxy.radius*10,
		minStep = Math.floor(galaxy.radius*0.125);

		var particleGroup = new SPE.Group({
			texture: t.particle,
			transparent: true,
			blending: THREE.AdditiveBlending,
			maxAge: 2
		});

		initCore(particleGroup, compositionColor, minStep);
		initBody(particleGroup, compositionColor, minStep);

		return particleGroup.mesh;
	}

	function initCore(particleGroup, compositionColor, minStep){
		var c = View.conf,
		emitter,
		q = c.settings.particlesQuality;

		if(q >= 1)
		{
			emitter = new SPE.Emitter({
				type: 'sphere',
				radius: minStep*2,
				radiusScale: new THREE.Vector3(0.5, 0.5 , 1.3),
				position: new THREE.Vector3(0, 0, 0),
				colorStart: compositionColor,
				sizeStart:25,
				sizeSpread:10,
				particleCount: 40,
				opacityStart: 0.9,
				isStatic: 1
			});

			particleGroup.addEmitter(emitter);


			emitter = new SPE.Emitter({
				type: 'sphere',
				radius: minStep*3,
				radiusScale: new THREE.Vector3(0.6, 0.3 , 1.3),
				position: new THREE.Vector3(0, 0, 0),
				colorStart: compositionColor,
				sizeStart:15,
				sizeSpread:10,
				particleCount: 40,
				opacityStart: 0.9,
				isStatic: 1
			});

			particleGroup.addEmitter(emitter);

			emitter = new SPE.Emitter({
				type: 'sphere',
				radius: minStep*4,
				radiusScale: new THREE.Vector3(0.7, 0.15 , 1.3),
				position: new THREE.Vector3(0, 0, 0),
				colorStart: compositionColor,
				sizeStart:8,
				sizeSpread:10,
				particleCount: 60,
				opacityStart: 0.9,
				isStatic: 1
			});
			particleGroup.addEmitter(emitter);

		}
		if(q >= 2)
		{
			
			emitter = new SPE.Emitter({
				type: 'sphere',
				radius: minStep*5,
				radiusScale: new THREE.Vector3(0.7, 0.1 , 1.3),
				position: new THREE.Vector3(0, 0, 0),
				colorStart: compositionColor,
				sizeStart:4,
				sizeSpread:10,
				particleCount: 5*minStep,
				opacityStart: 0.9,
				isStatic: 1
			});

			particleGroup.addEmitter(emitter);
		}
		if(q >= 3)
		{
			emitter = new SPE.Emitter({
				type: 'sphere',
				radius: minStep*5,
				radiusScale: new THREE.Vector3(0.7, 0.04 , 1.3),
				position: new THREE.Vector3(0, 0, 0),
				colorStart: new THREE.Color("white"),
				sizeStart:2,
				sizeSpread:3,
				particleCount: 5*minStep,
				opacityStart: 0.9,
				isStatic: 1
			});

			particleGroup.addEmitter(emitter);

			emitter = new SPE.Emitter({
				type: 'sphere',
				radius: minStep*6,
				radiusScale: new THREE.Vector3(0.7, 0.02 , 1.3),
				position: new THREE.Vector3(0, 0, 0),
				colorStart: new THREE.Color("white"),
				sizeStart:2,
				sizeSpread:2,
				particleCount: 5*minStep,
				opacityStart: 0.8,
				isStatic: 1
			});

			particleGroup.addEmitter(emitter);
		}
	}

	function initBody(particleGroup, compositionColor, minStep){
		var c = View.conf,
		emitter,
		q = c.settings.particlesQuality;

		//InnerBody
		if(q >= 1)
		{
			emitter = new SPE.Emitter({
				type: 'sphere',
				radius: minStep*29,
				radiusScale: new THREE.Vector3(1, 0.01 ,1),
				position: new THREE.Vector3(0, 0, 0),
				colorStart: new THREE.Color("rgb(250,125,125)"),
				sizeStart: 2,
				sizeStartSpread: 5,
				particleCount: (q*10*minStep)/2,
				opacityStart: 1,
				//opacitySpread:0.3, //0.3
				isStatic: 1
			});
			particleGroup.addEmitter(emitter);
			emitter = new SPE.Emitter({
				type: 'sphere',
				radius: minStep*29,
				radiusScale: new THREE.Vector3(1, 0.02 ,1),
				position: new THREE.Vector3(0, 0, 0),
				colorStart: new THREE.Color('white'),
				sizeStart: 1,
				sizeStartSpread: 4,
				particleCount: (q*10*minStep)*2,
				opacityStart: 1,
				//opacitySpread:0.3,
				isStatic: 1
			});
			particleGroup.addEmitter(emitter);
			emitter = new SPE.Emitter({
				type: 'sphere',
				radius: minStep*29,
				radiusScale: new THREE.Vector3(1, 0.01 ,1),
				position: new THREE.Vector3(0, 0, 0),
				colorStart: new THREE.Color("rgb(100,125,250)"),
				sizeStart: 3,
				sizeStartSpread: 5,
				particleCount: (q*10*minStep)/2,
				opacityStart: 1,
				//opacitySpread:0.3,
				isStatic: 1
			});
			particleGroup.addEmitter(emitter);
		}
		if(q >= 2)
		{
			//Outer
			emitter = new SPE.Emitter({
				type: 'sphere',
				radius:minStep*32,
				radiusScale: new THREE.Vector3(1, 0.02 ,1),
				position: new THREE.Vector3(0, 0, 0),
				colorStart: new THREE.Color("rgb(250,125,125)"),
				sizeStart: 3,
				sizeStartSpread: 5,
				particleCount: (q*10*minStep)/3,
				opacityStart: 1,
				//opacitySpread:0.3,
				isStatic: 1
			});
			particleGroup.addEmitter(emitter);
			emitter = new SPE.Emitter({
				type: 'sphere',
				radius: minStep*32,
				radiusScale: new THREE.Vector3(1, 0.03 ,1),
				position: new THREE.Vector3(0, 0, 0),
				colorStart: new THREE.Color('white'),
				sizeStart: 2,
				sizeStartSpread: 4,
				particleCount: (q*10*minStep),
				opacityStart: 1,
				isStatic: 1
			});
			particleGroup.addEmitter(emitter);
			emitter = new SPE.Emitter({
				type: 'sphere',
				radius: minStep*32,
				radiusScale: new THREE.Vector3(1, 0.02 ,1),
				position: new THREE.Vector3(0, 0, 0),
				colorStart: new THREE.Color("rgb(100,125,250)"),
				sizeStart: 3,
				sizeStartSpread: 5,
				particleCount: (q*10*minStep)/3 ,
				opacityStart: 1,
				//opacitySpread:0.2,
				isStatic: 1
			});
			particleGroup.addEmitter(emitter);
		}
		if(q >= 3)
		{
			//Outer
			emitter = new SPE.Emitter({
				type: 'sphere',
				radius:minStep*34,
				radiusScale: new THREE.Vector3(1, 0.02 ,1),
				position: new THREE.Vector3(0, 0, 0),
				colorStart: new THREE.Color("rgb(250,125,125)"),
				sizeStart: 3,
				sizeStartSpread: 5,
				particleCount: (q*10*minStep)/4,
				opacityStart: 1,
				//opacitySpread:0.3,
				isStatic: 1
			});
			particleGroup.addEmitter(emitter);
			emitter = new SPE.Emitter({
				type: 'sphere',
				radius: minStep*33,
				radiusScale: new THREE.Vector3(1, 0.03 ,1),
				position: new THREE.Vector3(0, 0, 0),
				colorStart: new THREE.Color('white'),
				sizeStart: 2,
				sizeStartSpread: 4,
				particleCount: (q*10*minStep),
				opacityStart: 1,
				isStatic: 1
			});
			particleGroup.addEmitter(emitter);
				emitter = new SPE.Emitter({
				type: 'sphere',
				radius: minStep*34,
				radiusScale: new THREE.Vector3(1, 0.02 ,1),
				position: new THREE.Vector3(0, 0, 0),
				colorStart: new THREE.Color("rgb(100,125,250)"),
				sizeStart: 3,
				sizeStartSpread: 5,
				particleCount: (q*10*minStep)/4 ,
				opacityStart: 1,
				//opacitySpread:0.2,
				isStatic: 1
			});
			particleGroup.addEmitter(emitter);
		}
	}
})();
