///////////////////////////////////////////////////////
///                  View.Lang                      ///
/// ------------------------------------------------///
/// View localization								///
/// ------------------------------------------------///
///////////////////////////////////////////////////////


var View = View || {};
(function(){
	"use strict";

	View.Lang  = {
		literals: {},
		currentLanguage : ""
	};

	View.Lang.init = function(cb){
		View.Lang.currentLanguage = navigator.language;
		$$.get(location.origin + "/getlang/"+View.Lang.currentLanguage,"",function(data){
			View.Lang.literals = data;
			View.Lang.translate();
			cb();
		});
	};

	View.Lang.translate = function(){
		var elements = document.querySelectorAll("[data-locale]"),
		key ="";

		for(var i = 0; i < elements.length; i++)
		{
			key = elements[i].dataset.locale;
			if(View.Lang.literals[key])
				elements[i].innerHTML = View.Lang.literals[key];
			
		}
		

	};
})();