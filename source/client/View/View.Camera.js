///////////////////////////////////////////////////////
///                 View.Camera                     ///
/// ------------------------------------------------///
/// Camera functions								///
/// ------------------------------------------------///
///////////////////////////////////////////////////////

/**
*@module View
*/

/**
* @class Camera
* @namespace View
*/
var View = View || {};
(function(){
	"use strict";

		View.unsetCameraConstraints= function(){
			this.setCameraConstraints(Const.CAMERA.NONE_CONSTRAINTS);
		};

		View.setCurrentConstraints= function(){
			this.setCameraConstraints(this.hud.globals.camConstraints);
		};

		/**
		* Disables user interaction with the camera
		*
		* @method disableControls
		* @namespace View.Camera
		*/
		View.disableControls= function(){
			setTimeout(function(){
				View.hud.globals.controls.enabled = false;
				
			},10);
			this.unsetCameraConstraints();
		};

		/**
		* Enables user interaction with the camera
		*
		* @method disableControls
		* @namespace View.Camera
		*/
		View.enableControls= function(){
			this.hud.globals.controls.enabled = true;
			this.setCurrentConstraints();
		};
		/**
		* Sets the camera's orbit center to specified coords
		* 
		* @method setCameraTarget
		* @param {THREE.Vector3} Target 3D coords
		* @namespace View.Camera
		*/
		View.setCameraTarget= function(target, cb){
			var gh = this.hud.globals;
			var tween = new TWEEN.Tween(gh.controls.center)
			.easing(TWEEN.Easing.Quadratic.InOut)
			.to(target, Const.CAMERA.GALAXY_MOVE_TIME)
			.onComplete(function(){
				if(cb) cb();
			})
			.start();
		};

		View.makeCameraChildOf = function(obj){
			obj.add(View.webgl.globals.camera);
		};

		/**
		* Sets the camera's position to specified coords
		* 
		* @method setCameraPosition
		* @param {THREE.Vector3} Target 3D coords
		* @param {Function} callback
		* @param int Easing constant. Default Quadratic.InOut
		*/
		View.setCameraPosition= function(target, cb, easing){
			var gh = this.hud.globals;
			var tween = new TWEEN.Tween(gh.controls.object.position)
			.easing(easing || TWEEN.Easing.Quadratic.InOut)
			.to(target, Const.CAMERA.GALAXY_MOVE_TIME)
			.onComplete(function(){
				if(cb) cb();
			})
			.start();
		};

		/**
		* Sets the camera constraints as min/maxZoom, min/maxPolarAngle,
		* userPan, etc.
		*
		* @method setCameraConstraints
		* @param {Object} Constraints as {constraint: String, val: value}
		* @namespace View.Camera
		*/
		View.setCameraConstraints= function(constraints){
			View.hud.globals.camConstraints = constraints;
			var controls = View.hud.globals.controls;
			constraints.forEach(function(c){
				controls[c.constraint] = c.val;
			});
		};

	
})();
