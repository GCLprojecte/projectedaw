/////////////////////////////
///         View           //
/// ---------------------- //
/// Canvas, HTML and event //
/// delegation             //
/////////////////////////////


/**
 * @module View
 */


var View = {};

/**
 * Initialization and configuration of View
 * 
 * @class View
 *
 * @requires Const
 * @requires THREE
 * @requires OrbitControls
 */
(function(){
	"use strict";
	View=
	{

		globals:{
			updateFunctions:[],
			clock: null,
			contextInitialised: false
		},
		/**
		* View configuration
		*
		* @property conf
		* @type Object 
		*/
		conf:{

			//Camera
			initCamPos: new THREE.Vector3(0,200, 200),
			dof: 45,
			near: 0.1,
			far: 15000,

			//Animation
			stop:false,

			//Orbit lines
			showOrbit:true,

			settings:{
				//TODO: galactivViewSettings / stellarViewSettings
				particlesQuality: 0,
				transitions: true,
				//Antialiasing
				aa: true,
				resolution: 1,
				anisotropy: 16,
				mipmaps:true
			}
		},

		webgl:{
			/**
			* Path to textures loaded on init
			*
			* @property initTexturesPath
			* @type Object 
			*/
			initTexturesPath:{
				galaxyLensFlare: 'img/lensFlare.png'
			},

			/**
			* Loaded textures. Once a texture is loaded you can find it here
			*
			* @property textures
			* @type Object 
			*/
			textures:{},

			/**
			* Resusable loaded geometries
			*
			* @property geometries
			* @type Object 
			*/
			geometries:{},

			/**
			* Scene globals. (Objects, cameras, lights...)
			* @property globals
			* @type Object
			*/
			globals: {
				galaxies:[],
				planets:[],
				orbits:[]
			}
		},
		
		hud:{
			globals: {
				galaxies:[],
				galaxyNames:[],
				planets:[]
			},
			sheet: {},
			state: {}

		},
		init:function(){
			if(!this.globals.contextInitialised)
			{
				this.initCss3D();
				this.initWebGL();
				this.globals.contextInitialised = true;
			}
			else
			{
				this.updateRendererResolution();
			}
			this.initHud();
			this.initScene();
			this.startRender();
			this.Events.init();
		},

		updateRendererResolution: function(){
			var gw = View.webgl.globals,
			renderer = View.webgl.globals.renderer,
			res = View.conf.settings.resolution;
			gw.renderer.setSize( window.innerWidth * res, window.innerHeight* res);
			gw.camera.aspect	= window.innerWidth / window.innerHeight;
			gw.camera.updateProjectionMatrix();
		},

		/**
		* Scene globals. (Objects, cameras, lights...)
		* 
		* @method initTextures
		* @param {Function} callback
		*/
		initTextures: function(cb){
			this.loadTextures(this.webgl.initTexturesPath, cb);
		},

		/**
		* Loads textures asynchronously and saves them in this.textures
		* 
		* @method loadTextures
		* @param {Function} callback
		*/
		loadTextures: function(texturesPath, cb){

			var textureList = [],
			index,
			t = this.webgl.textures,
			i=0,
			c = this.conf.settings,
			self = this,
			maxAnisotropy = this.webgl.globals.renderer.getMaxAnisotropy();

			for(index in texturesPath) 
			{ 
				if(texturesPath.hasOwnProperty(index)) 
				{
					//If texture is not loaded
					if(!t[index])
						textureList.push({index: index, path: texturesPath[index]});
				}
			}

			if(textureList.length > 0)
				t[textureList[i].index] = THREE.ImageUtils.loadTexture(textureList[i].path, new THREE.UVMapping(), function loadNext(){
					i++;
					if(i < textureList.length)
					{
						t[textureList[i].index] = THREE.ImageUtils.loadTexture(textureList[i].path, new THREE.UVMapping(),  loadNext);
						t[textureList[i].index].anisotropy = Math.min(c.anisotropy, maxAnisotropy);
						if(c.mipmaps)
						{
							t[textureList[i].index].magFilter = THREE.LinearFilter;	
							t[textureList[i].index].minFilter = THREE.LinearMipMapLinearFilter;
						}
					}
					else
					{
						if(cb) cb();
					}
				});
			else
				if(cb) cb();

			
		},

		/**
		* Creates a WebGL renderer into the specified container.
		* 
		* @method initWebGL
		* @param {HTMLDomObject} Container. Default: querySelector("#canvas")
		* 
		*/
		initWebGL: function(container){
			var g = this.webgl.globals;
			g.renderer = new THREE.WebGLRenderer({antialias: this.conf.settings.aa, alpha:true});
			g.renderer.setSize(window.innerWidth*this.conf.settings.resolution, window.innerHeight*this.conf.settings.resolution);
			g.container = container || document.getElementById( 'canvas' );
			g.container.appendChild(g.renderer.domElement);


		},
		
		/**
		* Creates a CSS3D renderer into the specified container.
		* 
		* @method initCss3D
		* @param {HTMLDomObject} Container. Default: querySelector("#hud")
		* 
		*/
		initCss3D: function(container){
			var g = this.hud.globals;
			g.renderer = new THREE.CSS3DRenderer();
			g.renderer.setSize(window.innerWidth, window.innerHeight);
			g.container = container || document.querySelector("#hud");
			document.getElementById( 'hud' ).appendChild( g.renderer.domElement );
		},

		/**
		* Creates boilerplate scene. 
		* 
		* @method initScene
		*/
		initScene: function(){
			var gw = this.webgl.globals,
			scene = new THREE.Scene(),
			camera = new THREE.PerspectiveCamera(this.conf.dof, window.innerWidth / window.innerHeight, this.conf.near, this.conf.far);
			
			camera.position = this.conf.initCamPos;
			camera.matrixAutoUpdate = true;

			gw.camera = camera;
			gw.scene = scene;

			THREEx.WindowResize(gw.renderer, gw.camera, this.conf.settings.resolution);

			scene.add(camera);

		},

		/**
		* Creates boilerplate hud. 
		* 
		* @method initHud
		*/
		initHud: function(){
			//Init
			var gh = this.hud.globals,
			gw = this.webgl.globals,
			camera = new THREE.PerspectiveCamera(this.conf.dof, window.innerWidth / window.innerHeight, this.conf.near, this.conf.far),
			controls = new THREE.OrbitControls(camera, gh.renderer.domElement),
			scene = new THREE.Scene(),
			sheet = (function() {
				var style = document.createElement("style");
				style.appendChild(document.createTextNode(""));
				document.head.appendChild(style);
				return style.sheet;
			})();

			//Conf
			if(!View.conf.settings.transitions)
				sheet.addRule("*","transition: none !important;");

			//Set
			camera.position =this.conf.initCamPos;
			
			//Save
			gh.camera = camera;
			gh.controls = controls;
			gh.scene = scene;
			gh.sheet = sheet;

			THREEx.WindowResize(gh.renderer, gh.camera);

			scene.add(camera);

			this.globals.updateFunctions.push(function(){
				gh.controls.update();
				gw.camera.position = gh.camera.position;
				gw.camera.rotation = gh.camera.rotation;
			});
		},

		/**
		* Empties the scene and stops the renderer;
		*
		* @method clearScene
		*/
		clearState: function(cb){
			this.stopRender();
			this.webgl.globals.scene.clear();
			this.hud.globals.scene.clear();
			this.globals.updateFunctions = [];
			this.webgl.globals.scene = {};
			this.webgl.globals.galaxies = [];
			this.webgl.globals.planets = [];
			this.webgl.globals.orbits=[];
			this.webgl.globals.halo="";
			this.hud.globals.galaxyNames = [];
			this.hud.globals.galaxies = [];
			this.hud.globals.planets = [];
			this.hud.sheet = {};
			this.hud.state={};
			this.Events.unregisterAll();
			this.clearInterface(cb);
		},

		reset: function(cb){
			this.clearState(cb);
			this.init();
		},


		/**
		* Starts animation loop
		* 
		* @method startRender
		*/
		startRender: function(){
			this.conf.stop = false;
			var self = this,
			g = this.globals;

			g.clock = new THREE.Clock();

			requestAnimationFrame(function(){self._renderLoop();});
		},

		/**
		* Stops animation loop
		* 
		* @method stopRender
		*/
		stopRender: function(){
			this.conf.stop = true;
		},

		/**
		* Runs update function's stack
		* 
		* @private
		* @method updateScene
		*/
		_updateScene: function(){
			var g = this.globals,
			delta = g.clock.getDelta();

			TWEEN.update();

			g.updateFunctions.forEach(function(updateFn){
				updateFn(delta);
			});
		},

		/**
		* Renders and updates the scene
		* 
		* @method renderLoop
		* @private
		*/
		_renderLoop: function(){
			var gw = this.webgl.globals,
			gh = this.hud.globals,
			self = this;
			if(!this.conf.stop)
			{

				gw.renderer.render(gw.scene, gw.camera);
				gh.renderer.render(gh.scene, gh.camera);
				this._updateScene();
				requestAnimationFrame(function(){self._renderLoop();});
			}

		},

		/**
		* Computates a RGB color based on a composition
		* 
		* @param {Array} Composition
		* @return {THREE.Color} color
		*/
		getCompositionColor: function(composition){
			var color = {r:0, g:0, b:0},
			colorElement,
			res;

			composition.forEach(function(element){
				if(Const.ELEMENT[element])
					colorElement = Const.ELEMENT[element];
				else if(Const.RARE_ELEMENT[element])
					colorElement = Const.RARE_ELEMENT[element];
				else
					return;

				color.r = (color.r + colorElement.r) / 2;
				color.g = (color.g + colorElement.g) / 2;
				color.b = (color.b + colorElement.b) / 2;
			});

			res = new THREE.Color();
			res.setRGB(
				(color.r * 100)/25500,
				(color.g * 100)/25500,
				(color.b * 100)/25500
				);

			return res;
		}
	};
})();
