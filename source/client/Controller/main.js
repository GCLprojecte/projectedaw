/////////////////////////////
///      Controller        //
/// ---------------------- //
/// Logic                  //
/////////////////////////////


/**
 * @module Controller
 */

var Controller = {};

(function(){
	"use strict";
	/**
	*	Controls the logic of the game
	*
	* @class Controller
	*/
	Controller=
	{
		/**
		* Current state
		*
		* @property state
		*/
		state:{
			universe : {},
			currentView : "",
			currentGalaxyId : "",
			currentSystemId: "",
			activeTab: "",
			settings:{}
		},
		/**
		*	Starts the game
		*
		*	@method init
		*/
		init: function(){
			this.loadSettings();
			if(this.state.settings.galactic)
				View.conf.settings = this.state.settings.galactic;
			View.init();
			var universe = this.getUniverse();

			View.drawUniverse(universe);
			this.state.universe = universe;
			Controller.setCurrentView(Const.VIEWS.UNIVERSE);
			setTimeout(function(){
				View.hideLoading();

			},10);
		},
		reset: function(){
			location.reload();
			/*
			this.state = {
				universe : {},
				currentView : "",
				currentGalaxyId : ""
			};
			View.clearState();
			Controller.init();*/
		},

		loadSettings: function(){
			if(localStorage.settings !== undefined){
				this.state.settings = JSON.parse(localStorage.settings);
				View.setSettings(this.state.settings);
			}
		},

		saveSettings: function(settings){
			localStorage.settings = JSON.stringify(settings);
		},

		settingsChanged: function(){
			var settings ={
				galactic:{
					resolution: document.getElementById("galacticViewResolution").value,
					particlesQuality: document.getElementById("galacticParticlesQuality").value,
					transitions: document.getElementById("galacticViewCssTransitions").checked,
					aa: document.getElementById("galacticViewAntialiasing").checked
				},
				stellar:{
					resolution: document.getElementById("stellarViewResolution").value,
					transitions: document.getElementById("stellarViewCssTransitions").checked,
					aa: document.getElementById("stellarViewAntialiasing").checked
				}
			};
			this.saveSettings(settings);
			this.state.settings = settings;

		},


		/**
		* Changes the view to the Solar system view
		*
		* @method systemOnClick
		* @param e Event
		* @context SolarSystem DOM Element
		*/
		systemOnClick: function(e){
			var id = this.dataset.system;
			Controller.state.currentSystemId = id;
			Controller.setCurrentView(Const.VIEWS.SOLAR_SYSTEM);
		},

		systemOnHover: function(e){
			var id = this.dataset.system,
			objs = document.querySelectorAll(
				".galacticObject[data-system="+id+"], "+
				"#i-rightMenu .content ul li[data-system="+id+"]"
			);

			for(var i = 0; i < objs.length; i++)
			{
				objs[i].className += " hover";
			}
		},

		systemOnMouseOut: function(e){
			var id = this.dataset.system, 
			objs = document.querySelectorAll(
				".galacticObject[data-system="+id+"], " +
				"#i-rightMenu .content ul li[data-system="+id+"]"
			);

			for(var i = 0; i < objs.length; i++)
			{
				objs[i].className = objs[i].className.replace(/ hover/g,"");
			}
		},

		/**
		* Changes the view to the Galaxy View
		*
		* @method galaxyOnClick
		* @param e Event
		* @context galaxy DOM Element <- (this)
		*/
		galaxyOnClick: function(e){
			var id = this.dataset.galaxy; //e.target.dataset.galaxy;
			Controller.state.currentGalaxyId = id;
			Controller.setCurrentView(Const.VIEWS.GALAXY);
		},

		/**
		* Gives both the galaxy and list element the Hover status 
		* when hovering in one of them
		*
		* @method galaxyOnHover
		* @param e Event
		* @context galaxy DOM Element 
		*/
		galaxyOnHover: function(e){
			var id = this.dataset.galaxy, //e.target.dataset.galaxy,
			objs = document.querySelectorAll(
				".galaxy[data-galaxy="+id+"], " +
				".glx-name[data-galaxy="+id+"], "+
				"#i-rightMenu .content ul li[data-galaxy="+id+"]"
			);

			for(var i = 0; i < objs.length; i++)
			{
				objs[i].className += " hover";
			}
		},

		/**
		* Removes both the galaxy and list element the Hover status 
		* when the mouse goes out
		*
		* @method galaxyOnHoverOut
		* @param e Event
		* @context galaxy DOM Element 
		*/
		galaxyOnMouseOut: function(e){
			var id = this.dataset.galaxy, //e.target.dataset.galaxy,
			objs = document.querySelectorAll(
				".galaxy[data-galaxy="+id+"], " +
				".glx-name[data-galaxy="+id+"], "+
				"#i-rightMenu .content ul li[data-galaxy="+id+"]"
			);

			for(var i = 0; i < objs.length; i++)
			{
				objs[i].className = objs[i].className.replace(/ hover/g,"");
			}
		},

		/**
		* Changes the view to the Planet View
		*
		* @method planetOnClick
		* @param e Event
		* @context planet DOM Element <- (this)
		*/
		planetOnClick: function(e){
			var id = this.dataset.planet; 
			Controller.state.currentPlanetId = id;
			Controller.setCurrentView(Const.VIEWS.PLANET);
		},

		/**
		* Gives both the planet and list element the Hover status 
		* when hovering in one of them
		*
		* @method planetOnHover
		* @param e Event
		* @context planet DOM Element 
		*/
		planetOnHover: function(e){
			var id = this.dataset.planet, 
			objs = document.querySelectorAll(
				".planet[data-planet="+id+"], " +
				"#i-rightMenu .content ul li[data-planet="+id+"]"
			);

			for(var i = 0; i < objs.length; i++)
			{
				objs[i].className += " hover";
			}
		},

		/**
		* Removes both the planet and list element the Hover status 
		* when the mouse goes out
		*
		* @method planetOnMouseOut
		* @param e Event
		* @context planet DOM Element 
		*/
		planetOnMouseOut: function(e){
			var id = this.dataset.planet, 
			objs = document.querySelectorAll(
				".planet[data-planet="+id+"], " +
				"#i-rightMenu .content ul li[data-planet="+id+"]"
			);

			for(var i = 0; i < objs.length; i++)
			{
				objs[i].className = objs[i].className.replace(/ hover/g,"");
			}
		},

		forcePlanetMouseOut: function(id){
			var objs = document.querySelectorAll(
				".planet[data-planet="+id+"], " +
				"#i-rightMenu .content ul li[data-planet="+id+"]"
			);

			for(var i = 0; i < objs.length; i++)
			{
				objs[i].className = objs[i].className.replace(/ hover/g,"");
			}
		},

		/**
		* Changes the view to the moon View
		*
		* @method moonplanetOnClick
		* @param e Event
		* @context moon DOM Element <- (this)
		*/
		moonOnClick: function(e){
			var id = this.dataset.moon,
			moon =Controller.getMoon(id);
			View.openMoonDialog(moon);
		},

		/**
		* Gives the Hover status 
		*
		* @method moonOnHover
		* @param e Event
		* @context moon DOM Element 
		*/
		moonOnHover: function(e){
			this.className += " hover";
			
		},

		/**
		* Removes  the Hover status when the mouse goes out
		*
		* @method moonOnMouseOut
		* @param e Event
		* @context moon DOM Element 
		*/
		moonOnMouseOut: function(e){
			this.className = this.className.replace(/ hover/g,"");
			
		},

		/**
		* Changes the view
		*
		* @method setCurrentView
		* @param String View
		*/
		setCurrentView: function(view){
			var s = this.state,
			galaxy,
			planet,
			lastGalaxy,
			lastSystem,
			lastPlanet,
			solarSystem;

			//If we already are on the requested view
			if(view === this.state.currentView) 
				return;

			//If the last view was galaxy view, make unset
			if(this.state.currentView == Const.VIEWS.GALAXY) {
				lastGalaxy = this.getCurrentGalaxy();
			}
			else if(this.state.currentView == Const.VIEWS.SOLAR_SYSTEM) {
				lastSystem = this.getCurrentSolarSystem();
			}
			else if(this.state.currentView == Const.VIEWS.PLANET)
			{
				lastPlanet = this.getCurrentPlanet();
			}
			
			s.currentView = view;

			if(view == Const.VIEWS.UNIVERSE)
			{
				if(lastGalaxy){
					s.currentGalaxyId="";

					View.unsetGalaxyView(lastGalaxy);
				}

				View.setUniverseView(s.universe);
			}
			else if(view == Const.VIEWS.GALAXY)
			{
					galaxy = this.getCurrentGalaxy();

				if(lastSystem){
					s.currentSystemId="";

					View.unsetSolarSystemView(lastSystem,galaxy);
				}
				else{
					View.setGalaxyView(galaxy);
					
				}
			}
			else if(view == Const.VIEWS.SOLAR_SYSTEM)
			{
				if(lastPlanet)
				{
					View.unsetPlanetView(this.getCurrentPlanet(),this.getCurrentSolarSystem());
					s.currentPlanetId="";
				}
				else
				{
					solarSystem = this.getCurrentSolarSystem();
					View.setSolarSystemView(solarSystem, lastGalaxy.coords);
				}
			}
			else if(view == Const.VIEWS.PLANET)
			{
				this.forcePlanetMouseOut(s.currentPlanetId);
				planet = this.getCurrentPlanet();
				View.setPlanetView(planet, lastSystem);
			}
		},

		/**
		* Sets upper view in hierarchy. (Ex: Galaxy then Universe)
		*
		* @method goBack
		*/
		goBack: function(){
			var s = this.state;
			if(s.currentView > 0)
				this.setCurrentView(this.state.currentView-1);
		},

		/**
		* Searches the galaxy in the universe and returns it.
		*
		* @method getGalaxy
		* @return Galaxy
		*/
		getCurrentGalaxy: function(){
			var galxRes = {},
			s = this.state;

			for(var i = 0; i < s.universe.galaxies.length; i++){
				if(s.universe.galaxies[i].id == s.currentGalaxyId){
					return s.universe.galaxies[i];
				}
			}
			return null;
		},

		/**
		* Searches the solar system in the universe and returns it.
		*
		* @method getSolarSystem
		* @return SolarSystem
		*/
		getCurrentSolarSystem: function(){
			var ssRes = {},
			s = this.state,
			systems = this.getCurrentGalaxy().galacticObjects;

			for(var j = 0; j < systems.length; j++)
			{
				if(systems[j].id == s.currentSystemId){
					return systems[j];
				}
			}

			return null;
		},

		/**
		* Searches the planet and returns it
		*
		* @method getPlanet
		* @return Planet
		*/
		getCurrentPlanet: function(){
			var plRes = {},
			s = this.state,
			planets = this.getCurrentSolarSystem().planets;

			for(var i = 0; i< planets.length; i++)
			{
				if(planets[i].id== s.currentPlanetId)
					return planets[i];
			}

			return null;
		},

		/**
		* Searches the moon and returns it
		*
		* @method getMoon
		* @return Moon
		*/
		getMoon: function(id){
			var planet = this.getCurrentPlanet(),
			moons = planet.moons;

			for(var i = 0; i < moons.length; i++)
			{
				if(moons[i].id == id)
					return moons[i];
			}
		},

		targetSun: function(){
			document.getElementById("i-btnPlanet").style.opacity=Const.CSS.BTN_OPACITY;
			document.getElementById("i-btnSun").style.opacity=Const.CSS.BTN_OPACITY_ACTIVE;
			View.setCameraConstraints(Const.CAMERA.SYSTEM_CONSTRAINTS);
			View.setCameraTarget(new THREE.Vector3());
		},

		targetPlanet: function(){
			document.getElementById("i-btnSun").style.opacity=Const.CSS.BTN_OPACITY;
			document.getElementById("i-btnPlanet").style.opacity=Const.CSS.BTN_OPACITY_ACTIVE;
			var planet = View.getWebglPlanet(this.state.currentPlanetId);
			View.setCameraTarget(planet.children[0].position, function(){
				View.setCameraConstraints(Const.CAMERA.PLANET_CONSTRAINTS);
			});
		},

		addGalaxy: function(){
			var newGalx = GOD.Galaxy.create();

			this.state.universe.galaxies.push(newGalx);
			localStorage.universe = JSON.stringify(this.state.universe);
			View.showLoading();
			setTimeout(function(){
				Controller.reset();
			},Const.CSS.SHOW_LOADING_TIME);
		},

		getUniverse: function(){
			var uni = "";
			if(localStorage.universe){
				var uniData = JSON.parse(localStorage.universe);
				uni = this.parseUniverse(uniData);
			}
			else{
				uni = GOD.Universe.create();
				localStorage.universe =JSON.stringify(uni);
			}


			return uni;
		},

		parseUniverse: function(uni){
			var parsedUni = new Universe();
			uni.galaxies.forEach(function(galx){
				var parsedGalx = new Galaxy(galx.id,galx.name,galx.type,galx.radius,galx.coords,galx.composition);
				galx.galacticObjects.forEach(function(go){
					var parsedGo = new SolarSystem(go.id, go.name, go.coords, go.type);
					go.planets.forEach(function(planet){
						var parsedPlanet = new Planet(planet.id, planet.name, planet.composition, planet.distance, 
							planet.radius, planet.type, planet.atmosfear, planet.surface, planet.temperature);
						planet.moons.forEach(function(moon){
							var parsedMoon = new Moon(moon.id, moon.name, moon.composition, moon.distance, moon.radius, 
								moon.type, moon.atmosfear, moon.surface, moon.temperature);
							parsedPlanet.moons.push(parsedMoon);
						});
						parsedGo.planets.push(parsedPlanet);
					});
					parsedGalx.galacticObjects.push(parsedGo);
				});
				parsedUni.galaxies.push(parsedGalx);
			});

			return parsedUni;
		},

		removeUniverse: function(){
			localStorage.universe = "";
		}
	};
})();

//Init
View.Events.onReady(function(){
	Controller.init();
});