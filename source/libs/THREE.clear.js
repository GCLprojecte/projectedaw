
// This THREE helper makes it easy to remove children
// # Code

//

/** @namespace */
var THREE	= THREE || {};

/**
 * Clears all children of the object
 *
 * @method clear
*/
THREE.Object3D.prototype.clear = function(){
    var children = this.children;
    for(var i = children.length-1;i>=0;i--){
        var child = children[i];
        child.clear();
        this.remove(child);
    }
};