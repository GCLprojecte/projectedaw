
/**
 * @venus-library mocha
 * @venus-include ../../node_modules/three/three.js
 * @venus-include ../../Universe/god.js
 * @venus-include ../../Universe/ColonizableObject.js
 * @venus-include ../../Universe/Moon.js
 * @venus-include ../../Universe/Planet.js
 * @venus-include ../../Universe/GalacticObject.js
 * @venus-include ../../Universe/SolarSystem.js
 * @venus-include ../../Universe/Util.js
 * @venus-include ../../Universe/Const.js
 */

describe('create',function(){
    it('should generate a solar system',function(){
        var ss = GOD.SolarSystem.create(
            Const.COLONIZABLE.BASE_COMPOSITION, 
            {x:0, y:0}
        );

        expect(ss).to.be.a(SolarSystem);
    });
});

describe('genType',function(){
    it('should generate a star type',function(){
        var types = Const.SOLAR_SYSTEM.TYPES;
        var gentype = GOD.SolarSystem.genType();

        expect(types).to.be.contain(gentype);
    });
});

describe('genColonizableObjects',function(){
    it('should generate colonizable objects',function(){
        var co = GOD.SolarSystem.genColonizableObjects(
            Const.COLONIZABLE.BASE_COMPOSITION, 
            'O'
        );

        expect(co.length).to.be.above(2);
    });
});