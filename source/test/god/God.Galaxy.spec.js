
/**
 * @venus-library mocha
 * @venus-include ../../node_modules/three/three.js
 * @venus-include ../../Universe/ColonizableObject.js
 * @venus-include ../../Universe/GalacticObject.js
 * @venus-include ../../Universe/god.js
 * @venus-include ../../Universe/Moon.js
 * @venus-include ../../Universe/Planet.js
 * @venus-include ../../Universe/Galaxy.js
 * @venus-include ../../Universe/SolarSystem.js
 * @venus-include ../../Universe/BlackHole.js
 * @venus-include ../../Universe/Util.js
 * @venus-include ../../Universe/Const.js
 */

describe('create',function(){
    it('should generate a galaxy',function(){
        var glx = GOD.Galaxy.create();

        expect(glx).to.be.a(Galaxy);
    });
});

describe('genGalaxyType',function(){
    it('should generate a galaxy type',function(){
        var glxtype = GOD.Galaxy.genType();
        var types = Const.GALAXY.TYPES;

        expect(types).to.contain(glxtype);

    });
});

describe('genGalaxyRadius',function(){
    it('should generate a galaxy radius between min and max',function(){
        var radius = GOD.Galaxy.genRadius();

        expect(radius).to.be.above(Const.GALAXY.MIN_RADIUS-1);
        expect(radius).to.be.below(Const.GALAXY.MAX_RADIUS+1);
    });

});

describe('genGalaxyCoords',function(){
    it('should generate galactic coordinates',function(){
        var coords = GOD.Galaxy.genCoords();

        expect(coords.x).to.be.a('number');
        expect(coords.y).to.be.a('number');
    });
});

describe('genComposition',function(){
    it('should generate a composition',function(){
        var compo = GOD.Galaxy.genComposition();
        var elementsList = Util.objToArray(Const.ELEMENT);

        compo.forEach(function(el){
            expect(elementsList).to.contain(el);
        });
    });
});

describe('genGalacticObjects',function(){
    it('should generate a constant number of galacticObjects',function(){
        var go = GOD.Galaxy.genGalacticObjects(['H', 'C', 'O']);
        var constantLength = Const.GALAXY.BLACK_HOLE_QUANT + Const.GALAXY.SOLAR_SYS_QUANT;
        expect(go.length).to.be(constantLength);
    });
});