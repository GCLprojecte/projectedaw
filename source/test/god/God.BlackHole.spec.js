
/**
 * @venus-library mocha
 * @venus-include ../../node_modules/three/three.js
 * @venus-include ../../Universe/god.js
 * @venus-include ../../Universe/GalacticObject.js
 * @venus-include ../../Universe/BlackHole.js
 * @venus-include ../../Universe/Util.js
 * @venus-include ../../Universe/Const.js
 */

describe('create',function(){
    it('should generate a black hole',function(){
        var bh = GOD.BlackHole.create();

        expect(bh).to.be.a(BlackHole);
    });
});

describe('genHorizon', function(){
    it('should generate an event horizon above 10',function(){
        var eh = GOD.BlackHole.genHorizon();

        expect(eh).to.be.above(10);
    });
});