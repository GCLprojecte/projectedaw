/*jshint expr: true*/

/**
 * @venus-library mocha
 * @venus-include ../node_modules/three/three.js
 * @venus-include ../Universe/View.js
 * @venus-include ../libs/OrbitControls2.js
 * @venus-include ../Universe/View.Galaxy.js
 * @venus-include ../Universe/Const.js
 * @venus-fixture ./fixtures/canvas.fixture.html
 */

describe('View',function(){
    describe('initWebGL',function(){
        it('should create a canvas element',function(){
            View.initWebGL();
            var containerLength = document.querySelector("#canvas").children.length;

            expect(containerLength).to.be(1);
        });
    });

    describe('initScene',function(){
        it('should create a basic scene',function(){
            View.initScene();
            var scene = View.globals.scene;

            expect(scene).to.be.an(THREE.Scene);
        });
    });

    describe('startRender',function(){
        it('should start rendering',function(){
            View.startRender();
            var stop = View.conf.stop;

            expect(stop).to.be.false;
        });
    });

    describe('stopRender',function(){
        it('should stop rendering',function(){
            View.stopRender();
            var stop = View.conf.stop;

            expect(stop).to.be.true;
        });
    });

    describe('getCompositionColor',function(){
        it('should compute RGB color based on composition',function(){
            var color = View.getCompositionColor(['C']);

            expect(+color.r.toFixed(2)).to.be(0.1);
            expect(+color.g.toFixed(2)).to.be(0.04);
            expect(+color.b.toFixed(2)).to.be(0.06);
        });
    });

    describe('setCameraTarget',function(){
        it('should place the camera target',function(){
            var point = {position:new THREE.Vector3(150, 10, -10)};

            View.setCameraTarget(point);
            var cameraTarget = View.globals.controls.center;

            expect(cameraTarget.x).to.be(150);
            expect(cameraTarget.y).to.be(10);
            expect(cameraTarget.z).to.be(-10);
        });
    });

});
