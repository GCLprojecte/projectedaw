
/**
 * @venus-library mocha
 * @venus-include ../Universe/Universe.js
 */

describe('Universe', function(){

    var uni;
    uni = new Universe();

    it('should be an universe',function(){

        expect(uni).to.be.a(Universe);
    });


    it('should remove a fleet',function(){
        var fleet = {id:'asdf',name:"fdsa"};
        uni.removeFleet('asdf');

        expect(uni.fleets[fleet.id]).to.not.be.ok();
    });
});
