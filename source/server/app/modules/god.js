/////////////////////////////
///          GOD           //
/// ---------------------- //
/// Generates random data  //
/////////////////////////////

/**
 * @module Core
 */

 var Util = require('../../../shared/Core/Util.js');
 var Const = require('../../../shared/Core/Const.js');
 var Planet = require ('../../../shared/Core/Planet.js');
 var Moon = require ('../../../shared/Core/Moon.js');
 var Galaxy = require ('../../../shared/Core/Galaxy.js');
 var SolarSystem = require ('../../../shared/Core/SolarSystem.js');
 var Universe = require ('../../../shared/Core/Universe.js');

/**
 * Generació d'Objectes Diferents.
 * Singleton
 * 
 * @class GOD
 *
 * @requires Const
 */
var GOD = {
    
    Universe:{
        create:function(){
            var galaxy = GOD.Galaxy.create("Nova galaxia");
            return new Universe(galaxy);    
        }
    },
    /**
    * Procedural galaxy generation
    * 
    * @namespace GOD
    * @class Galaxy
    */
    Galaxy:{
        /**
        * Creates random galaxy
        * 
        * @method create
        * @param String name
        * @return Galaxy
        */
        create:function(name)
        {
            var composition = this.genComposition(),
            radius = this.genRadius(),
            coords = this.genCoords(),
            objects = this.genGalacticObjects(composition, radius),
            type = this.genType(),
            id = Util.genId(),
            gname = name || "Galaxia";

            return new Galaxy(id,gname,type,radius,coords,composition, objects);
        },

        /**
        *  Returns a String representing a shape type.
        *
        * @method genType
        * @return String Type
        */
        genType: function(){

            var randomIndex = Util.randomInt(0, Const.GALAXY.TYPES.length-1);
            
            return Const.GALAXY.TYPES[randomIndex];
        },

        /**
        * Returns a number around 80 representing a galaxy diameter in
        * mpc (MegaParsecs)
        *
        * @method genRadius
        * @return int Radius
        */
        genRadius: function(){
            return Math.min(Math.max(Util.normalDist(20,80), Const.GALAXY.MIN_RADIUS), Const.GALAXY.MAX_RADIUS);
        },

        /**
        * Returns random galactic coords
        *
        * @method genCoords
        * @return Object Coords
        */
        genCoords: function(){
            var x, y;
            x = Util.randomInt(-1000, 1000);
            y = Util.randomInt(-1000, 1000);

            return {
                x: x,
                y: y
            };
        },

        /**
        * Returns random composition (3 elements array from Const.ELEMENT)
        * 
        * @method genComposition
        * @return {String[]} Composition
        */
        genComposition:function(){
            var elements = Util.objToArray(Const.ELEMENT),
            composition=[],
            randomIndex;

            for(var i = 0; i < 3; i++){
                randomIndex = Util.randomInt(0, elements.length-1);
                composition.push(elements[randomIndex]);
            }

            return composition;
        },
        /**
         * Generates all galactic objects of the galaxy
         *
         * @method generateGalacticObjects
         * @param {String[]} Composition
         */
        genGalacticObjects: function(composition, radius){
            var curr,
                galacticObjects = [],
                i,j;


            //Generating solar systems..
            for(i =0; i < Const.GALAXY.SOLAR_SYS_QUANT; i++){
                curr = GOD.SolarSystem.create(composition, radius);
                galacticObjects[i] = curr;
            }

            //Generating black holes..
            /*for(j =i; j-i < Const.GALAXY.BLACK_HOLE_QUANT; j++){
                curr = GOD.BlackHole.create();
                galacticObjects[j] = curr;
            }*/

            return galacticObjects;
        },
    },
    /**
    * Galactic object generation class
    *
    * @class GalacticObject
    * @namespace GOD
    */
    GalacticObject:{
        /**
        * Returns random coords
        *
        * @returns {x, y}
        */
        genCoords: function(galaxyRadius){
            var x= 0, y = 0, l = Const.GALAXY.COORDS_LIMIT;

            while(x > -l && x < l)
            {
                x = Util.randomInt(-galaxyRadius, galaxyRadius);
            }
            while(y > -l && y < l)
            {
                y = Util.randomInt(-galaxyRadius, galaxyRadius);
            }
            
            return {
                x: x,
                y: y
            };
        },
    },
    /**
    * Solar system generation
    *
    * @class SolarSystem
    * @namespace GOD
    */
    SolarSystem:{
        /**
        * Generates Solar system 
        *
        * @method create
        * @param {String[]} Composition
        * @return SolarSystem
        */
        create:function(composition, galaxyRadius){
            var id = Util.genId(),
                name = "system "+id,
                coords = GOD.GalacticObject.genCoords(galaxyRadius),
                type = this.genType(),
                objects = this.genColonizableObjects(composition, type);
            return new SolarSystem(id, name, coords, type, objects);  
        },
        /**
         * Star type of the solar system.
         *
         * @method genType
         * @returns {String}
         */
        genType: function(){
            var types = Const.SOLAR_SYSTEM.TYPES;

            return types[Util.randomInt(0, types.length-1)];
        },

        /**
        * Generates planets and moons
        *
        * @method genColonizableObjects
        * @return ColonizableObjects[]
        */
        genColonizableObjects: function(composition, type){
            var planetNum = Util.randomInt(Const.SOLAR_SYSTEM.MIN_PLANET_NUM,Const.SOLAR_SYSTEM.MAX_PLANET_NUM),
            info = Const.SOLAR_SYSTEM.SUN[type],
            basePlanet = false,
            currPlanet = {},
            planets = [],
            lastDistance=info.radius,
            distance,
            mass;

            for(var i = 1; i <= planetNum; i++)
            {
                distance = Math.max(lastDistance+Util.normalDist(100,100), lastDistance+Const.SOLAR_SYSTEM.MIN_DISTANCE);
                mass = Math.max(Util.normalDist(40,90)*Math.sin((i*10) * (Math.PI/180)), Const.COLONIZABLE.MIN_PLANET_MASS);

                //If distance is in the habitable zone
                if(info.habitableDist - Const.SOLAR_SYSTEM.HABITABLE_DIST_THRESHOLD < distance &&
                info.habitableDist + Const.SOLAR_SYSTEM.HABITABLE_DIST_THRESHOLD > distance)
                {
                    //If no base planet has yet created
                    if(!basePlanet)
                    {
                        currPlanet =GOD.Planet.createBase(composition, distance, type, mass);
                        basePlanet = true;
                    }
                    else
                    {
                        currPlanet =GOD.Planet.create(composition, distance, type, mass);
                    }
                }
                //If distance went beyond the habitable zone
                else if(info.habitableDist + Const.SOLAR_SYSTEM.HABITABLE_DIST_THRESHOLD < distance)
                {
                    //And no basePlanet was created
                    if(!basePlanet)
                    {
                        distance = info.habitableDist;

                        //If collides with previous planet, remove it
                        if(distance < lastDistance) planets.pop();

                        currPlanet =GOD.Planet.createBase(composition, distance, type, mass);
                        basePlanet = true;
                    }
                    else
                    {
                        currPlanet =GOD.Planet.create(composition, distance, type, mass);
                    }
                }
                else
                {
                    currPlanet =GOD.Planet.create(composition, distance, type, mass);
                }
                var moonlen=currPlanet.moons.length;
                if(moonlen > 0)
                {
                    currPlanet.distance += currPlanet.moons[moonlen-1].distance + 20;
                }
                planets.push(currPlanet);
                lastDistance = distance + (currPlanet.moons.length ? 
                    currPlanet.moons[currPlanet.moons.length-1].distance + 
                    currPlanet.moons[currPlanet.moons.length-1].radius + 
                    currPlanet.radius + 20 : 0);

            }

            return planets;
        }
    },
    /**
    * Black hole generation
    *
    * @class BlackHole
    * @namespace GOD
    */
    BlackHole:{
        /**
        * Generates a black hole
        *
        * @method create
        * @return BlackHole
        */
        create:function(){
            var id = Util.genId(),
            name = "black hole "+id,
            coords = GOD.GalacticObject.genCoords(),
            horizon = this.genHorizon();
            return new BlackHole(id, name, coords, horizon);
        },

        genHorizon: function(){
            return 11;
        }
    },
    /**
    * Colonizable objects abstract creation class
    * 
    * @class ColonizableObject
    * @namespace GOD
    */
    ColonizableObject:{
        /**
        * Generates random composition with a bigger chance of getting
        * same elements of the given composition
        *
        * @method genComposition
        * @param Array galaxyComposition
        * @return Array composition
        */
        genComposition: function(galaxyComposition){
            var composition =[],
            randomIndex,
            elements = Util.objToArray(Const.ELEMENT).concat(galaxyComposition),
            rare = Util.objToArray(Const.RARE_ELEMENT),
            i;    

            for(i = 0; i < 3; i++){
                randomIndex = Util.randomInt(0, elements.length-1);
                composition.push(elements[randomIndex]);
            }

            for(i=0; i < Util.randomInt(0,3); i++)
            {
                randomIndex = Util.randomInt(0, rare.length);
                composition.push(rare[randomIndex]);
            } 

            return composition;
        },

        /**
        * Radius based on given mass and type
        *
        * @method genRadius
        * @param int Planet type
        * @param int mass
        */
        genRadius: function(type, mass){
            var types = Const.COLONIZABLE.TYPES;
            switch(type)
            {
                case types.SOLID:
                    return mass;
                case types.GAS:
                    return mass*1.5;
                case types.ROCK:
                    return mass*0.66;
            }
        },

        /**
        * Type of the planet based on the composition.
        * (1 - Solid: Liquid core, rocky surface, atmosfear, ocean)
        * (2 - Rock: Rocky core and surface, no atmosfear)
        * (3 - Gas: Metallic core, liquid/gas surface, big atmosfear)
        *
        * @param Array composition
        * @return int Type
        */
        genType: function(composition){
            var gas=0, solid=0, rock=0, 
            els = Const.ELEMENT,
            rare = Const.RARE_ELEMENT;

            for(var i = 0; i < composition.length; i++)
            {
                if(els[composition[i]])
                {
                    if(els[composition[i]].type == Const.COLONIZABLE.TYPES.SOLID)
                    {
                        gas--;
                        solid+=2;
                        rock++;
                    }
                    else if(els[composition[i]].type == Const.COLONIZABLE.TYPES.GAS)
                    {
                        gas+=2;
                        rock--;
                    }
                    else
                    {
                        gas--;
                        solid++;
                        rock+=2;
                    }
                }
                else if(rare[composition[i]])
                {
                    if(rare[composition[i]].type == Const.COLONIZABLE.TYPES.SOLID)
                    {
                        gas--;
                        solid++;
                    }
                    else if(rare[composition[i]].type == Const.COLONIZABLE.TYPES.GAS)
                    {
                        gas++;
                        rock--;
                    }
                    else
                    {
                        gas--;
                        rock++;
                    }
                }
                

            }
            
            if(gas > solid)
            {
                if(rock > gas)
                    return Const.COLONIZABLE.TYPES.ROCK;
                else
                    return Const.COLONIZABLE.TYPES.GAS;
            }
            else
            {
                if(rock > solid)
                    return Const.COLONIZABLE.TYPES.ROCK;
                else
                    return Const.COLONIZABLE.TYPES.SOLID;
            }
        },

        /**
        * Generates atmosfear type ebased on planet type and composition
        *
        * @method genAtmosfear
        * @param int Planet type
        * @param Array Composition
        */
        genAtmosfear: function(type, composition){
            if(type ==  Const.COLONIZABLE.TYPES.ROCK)
                return Const.COLONIZABLE.ATMOSFEAR.NONE;
            else if(type == Const.COLONIZABLE.TYPES.SOLID)
            {
                if(composition.indexOf('O') != -1)
                    return Const.COLONIZABLE.ATMOSFEAR.BREATHABLE;
            }

            return Const.COLONIZABLE.ATMOSFEAR.NON_BREATHABLE;
        },

        /**
        * Generates edificable surface of the planet 
        * based on radius and the type. 
        *
        * @method genSurface
        * @param int radius
        * @param int Planet type
        */
        genSurface: function(radius, type){
            var surface = 4 * Math.PI * radius*radius;

            if(type == Const.COLONIZABLE.TYPES.ROCK)
                return surface; //surface as is

            else if(type == Const.COLONIZABLE.TYPES.SOLID)
                return surface - (surface/3)*2; //Surface - oceans

            else
                return surface*3; //Tridimensional surface
        },
        /**
        * Temperature based on the planet type and the sun
        *
        * @method genTemperature
        * @param int planet type
        * @param int sun type
        */
        genTemperature: function(type, distance, sun){
            var temperature = {min:0, max:0};

            if(type == Const.COLONIZABLE.TYPES.ROCK)
            {
                temperature.min = Math.max(-100 + Util.randomInt(-50,100) * distance, Const.ABSOLUTE_ZERO);
                temperature.max = 100 + 100/distance + Util.randomInt(0,100);
            }
            else if(type == Const.COLONIZABLE.TYPES.SOLID)
            {
                temperature.min = Util.randomInt(-50,0) + distance/100;
                temperature.max = Util.randomInt(0,50) + distance/100;
            }
            else
            {
                temperature.min = Util.randomInt(-50,0) + distance/100;
                temperature.max = Util.randomInt(0,50) + distance/100;
            }

            return temperature;
        }
    },

    /**
    * Planet creation class
    * 
    * @class Planet
    * @namespace GOD
    */
    Planet:{
        /**
        * Generates random planet based on the galaxyComposition, distance
        * to sun and a given mass
        * 
        * @method create
        * @param Array galaxyComposition
        * @param int Distance to sun
        * @param int sun type
        * @param int Planet mass
        * @return Planet new planet
        */
        create: function(galaxyComposition, distance, sun, mass){
            var id = Util.genId(),
            name = "Planet "+id,
            composition = GOD.ColonizableObject.genComposition(galaxyComposition),
            moonsMass = this.genMoonsMass(mass),
            planetMass = mass - moonsMass,
            type = GOD.ColonizableObject.genType(composition),
            radius = GOD.ColonizableObject.genRadius(type, planetMass),
            atmosfear = GOD.ColonizableObject.genAtmosfear(type, composition),
            surface = GOD.ColonizableObject.genSurface(radius, type),
            temperature = GOD.ColonizableObject.genTemperature(type, distance, sun),
            moons = [];
            
            if(moonsMass > 0){
                moons = this.genMoons(composition, moonsMass, distance, sun);
            }

            return new Planet(id, name, composition, distance, radius, type, atmosfear, surface, temperature, moons);
        },

        /**
        * Generates an habitable planet 
        * 
        * @method createBase
        * @param Array galaxyComposition
        * @param int Distance to sun
        * @param int sun type
        * @param int Planet mass
        * @return Planet new planet
        */
        createBase: function(galaxyComposition, distance, sun, mass){
            var id = Util.genId(),
            name = "Planet "+id,
            composition = Const.COLONIZABLE.BASE_COMPOSITION,
            moonsMass = this.genMoonsMass(mass),
            planetMass = mass,
            type = Const.COLONIZABLE.TYPES.SOLID,
            radius = GOD.ColonizableObject.genRadius(type, planetMass),
            atmosfear = Const.COLONIZABLE.ATMOSFEAR.BREATHABLE,
            surface = GOD.ColonizableObject.genSurface(radius, type),
            temperature = GOD.ColonizableObject.genTemperature(type, distance, sun),
            moons = [];
            
            if(moonsMass > 0)
                moons = this.genMoons(composition, moonsMass, distance, sun);

            return new Planet(id, name, composition, distance, radius, type, atmosfear, surface, temperature, moons);
        },

        /**
        * Generates a random mass for moons. If the mass is incompatible, returns 0
        * 
        * @method genMoonsMass
        * @param int mass
        * @return int Moons mass
        */
        genMoonsMass: function(mass){
            var rmass = Util.randomInt(0, mass);
            if(rmass >= Const.COLONIZABLE.MIN_MOON_MASS && (mass-rmass) >= mass/1.3)
            {
                return rmass;
            }
            else
                return 0;
        },
        /**
        * Generates moons based on the planet composition and a given mass
        *
        * @method genMoons
        * @param {String[]} Planet composition
        * @param int Moons' mass
        * @param int Distance to sun
        * @param String sun type
        * @return Moon[] Planet's moons
        */
        genMoons: function(composition, moonsMass, sunDistance, sun){
            var minMass = Const.COLONIZABLE.MIN_MOON_MASS,
            maxMass = Const.COLONIZABLE.MAX_MOON_MASS,
            moonNumber = Math.max(Math.floor((moonsMass / minMass)), 1),
            individualMoonMass = 0,
            randomAdditiveMass = 0,
            moons = [];

            for(i = 0; i < moonNumber; i++)
            {

                individualMoonMass = Util.randomInt(minMass, maxMass);
                distance = 2 + (moons[i-1] ? moons[i-1].distance : 0 ) + Util.randomInt(0,5)/10;
                moons[i] = GOD.Moon.create(composition, distance, individualMoonMass, sunDistance, sun);
                
            }

            return moons;
        }
    },
    /**
    * Moon creation class
    *
    * @class Moon
    * @namespace GOD
    */
    Moon:{
        /**
        * Creates a moon based on Planet composition and a given mass
        *
        * @method create
        * @param Array composition
        * @param int mass
        * @param int Distance to sun
        * @param String sun type
        */
        create: function(baseComposition, distance, mass, sunDistance, sun){
            var id = Util.genId(),
            name = "Moon "+id,
            composition = this.genComposition(baseComposition),
            type = GOD.ColonizableObject.genType(baseComposition),
            radius = this.genRadius(type, mass),
            atmosfear = GOD.ColonizableObject.genAtmosfear(type, composition),
            surface = GOD.ColonizableObject.genSurface(radius, type),
            temperature = GOD.ColonizableObject.genTemperature(type, sunDistance, sun);

            return new Moon(id, name, composition, distance, radius, type, atmosfear, surface, temperature);
        },
        genComposition: function(composition){
            var elements = Util.objToArray(Const.ELEMENT);
            composition.pop();
            composition.push(elements[Util.randomInt(0, elements.length-1)]);
            return composition;
        },
        genRadius: function(type, mass){
            var types = Const.COLONIZABLE.TYPES;
            switch(type)
            {
                case types.SOLID:
                    return mass;
                case types.GAS:
                    return mass*1.1;
                case types.ROCK:
                    return mass*0.66;
            }
        }
    }
    

};

module.exports = GOD;

