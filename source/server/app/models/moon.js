var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

//var Temperature = require('./temperature.js');

var moonSchema = new Schema({

	id 				: String,			//Id del objeto
	name 			: String,			//Nombre
	composition		: [String],			//Array de String, componentes
	distance		: Number,			//Distancia
	radius			: Number,			//Radio
	type 			: Number,			//Tipo
	atmosfear 		: Number, 			//Atmosfera
	surface 		: Number,			//Superficie
	//Objeto Temperatura
	temperature 	: { 				//Temperatura
						"min":Number, 
						"max":Number 
					  }	

});


module.exports = mongoose.model('Moon', moonSchema);