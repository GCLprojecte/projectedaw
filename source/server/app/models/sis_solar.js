var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var Planeta = require('./planeta.js');

var sis_solarSchema = new Schema({

	id 			: String,			//Id del objeto
	coords  	: { x: Number, y: Number },			//Objeto con las coordenadas
	name    	: String,			//Nombre
	type 		: String,			//Tipo
	planets 	: [{ type: Schema.ObjectId, ref:'Planeta'}]			//Array de objetos Planeta
	
});


module.exports = mongoose.model('SSolar', sis_solarSchema);