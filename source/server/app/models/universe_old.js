var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var Galaxy = require('./galaxy.js');

var universeSchema = new Schema({
	
	//_id			: false,			//No crear id de mongoose
	//id			: String,			//Id del objeto
	//Array de objetos Galaxy
	galaxies : [
		{
			id : String,
			name : String,
			composition : [String],
			type : String,
			radius : Number,
			coords : { x: Number, y: Number },
			fleets : {},
			galacticObjects: [
				{
					id : String,
					coords : { x : Number, y : Number },
					name : String,
					type : String,
					planets : [
						{
							id : String,
							name : String,
							composition : [String],
							distance : Number,
							radius : Number,
							type : Number,
							atmosfear : Number,
							surface : Number,
							temperature : { min : Number, max: Number},
							moons : [
								{
									id : String,
									name : String,
									composition : [String],
									distance : Number,
									radius : Number,
									type : Number,
									atmosfear : Number,
									surface : Number,
									temperature : { min : Number, max: Number}
								}
							]
						}
					]
				}
			]
		}
	]

});


module.exports = mongoose.model('Universe', universeSchema);