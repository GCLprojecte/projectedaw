var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var Galaxy = require('./galaxy.js');

var universeSchema = new Schema({
	
	//Array de objetos Galaxy
	galaxies:[]
	//galaxies : [{ type: Schema.ObjectId, ref:'Galaxy'}]

});


module.exports = mongoose.model('Universe', universeSchema);