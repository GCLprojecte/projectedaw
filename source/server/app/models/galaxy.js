var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var GalacticObject = require('./galacticObject.js');

var galaxySchema = new Schema({

	id 					: String,			//Id del objeto
	name				: String,			//Nombre de la galaxia
	composition 		: [String],			//Array de String, composiciones
	type 				: String, 			//Tipo
	radius              : Number, 			//Radio
	coords				: { x: Number, y: Number },			//Objeto Coords
	fleets				: [],				//Fleets
	//galacticObjects		: [{ type: Schema.ObjectId, ref:'GalacticObject'}]
	galacticObjects		: []	//Array objeto GalacticObject

});

module.exports = mongoose.model('Galaxy', galaxySchema);