var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var Moon = require('./moon.js');

var planetaSchema = new Schema({

	id 				: String,			//Id del objeto
	user			: String,			//Id del usuario propietario
	name 			: String,			//Nombre
	composition		: [String],			//Array de String, componentes
	distance		: Number,			//Distancia
	radius			: Number,			//Radio
	type 			: Number,			//Tipo
	atmosfear 		: Number, 			//Atmosfera
	surface 		: Number,			//Superficie
	temperature 	: { 				//Temperatura
						"min":Number, 
						"max":Number 
					  },
    owner			: String, 			//Usuari propietari del planeta
	moons			: [{ type: Schema.ObjectId, ref:'Moon', Default:[]}]			//Array de objetos Moon

});


module.exports = mongoose.model('Planeta', planetaSchema);