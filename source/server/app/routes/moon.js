//File: routes/moon.js
module.exports = function(app) {

  var Moon = require('../models/moon.js');

  //GET - Devuelve todos los mooons de la BD
  findAllMoons = function(req, res) {
    Moon.find(function(err, moon) {
      if(!err) {
        console.log('GET /moons')
        res.send(moon);
      } else {
        console.log('ERROR: ' + err);
      }
    });
  };

  //GET - Devuelve el moon con la ID especificada
  findMoonById = function(req, res) {
    Moon.find({id : req.params.id}, function(err, moon) {
      if(!err) {
        console.log('GET /moon/' + req.params.id);
        res.send(moon);
      } else {
        console.log('ERROR: ' + err);
      }
    });
  };

  //POST - Inserta un moon en La BD
  addMoon = function(req, res) {
    console.log('POST');
    console.log(req.body);

    var moon = new MooN({

      _id         : req.body.id,
      id          : req.body.id,
      name        : req.body.name,
      composition : req.body.composition,
      distance    : req.body.distance,
      radius      : req.body.radius,
      type        : req.body.type,
      atmosfear   : req.body.atmosfear,
      surface     : req.body.surface,
      temperature : req.body.temperature

    });

    moon.save(function(err) {
      if(!err) {
        //res.send("Moon creado");
        console.log('Moon creado');
      } else {
        //res.send("ERROR" + err);
        console.log('ERROR: ' + err);
      }
    });

    res.send(moon);
  };

  //PUT - Actualiza un registro de la id especificada
  updateMoon = function(req, res) {
    Moon.findById(req.params.id, function(err, moon) {
      
      moon.id          = req.body.id,
      moon.name        = req.body.name,
      moon.composition = req.body.composition,
      moon.distance    = req.body.distance,
      moon.radius      = req.body.radius,
      moon.type        = req.body.type,
      moon.atmosfear   = req.body.atmosfear,
      moon.surface     = req.body.surface,
      moon.temperature = req.body.temperature,
      moon.moons       = req.body.moons

      moon.save(function(err) {
        if(!err) {
          console.log('Moon Actualizado');
        } else {
          console.log('ERROR: ' + err);
        }
        res.send(moon);
      });
    });
  }

  //DELETE - Elimina el moon de la id especificada
  deleteMoon = function(req, res) {
    Moon.findById(req.params.id, function(err, moon) {
      moon.remove(function(err) {
        if(!err) {
          console.log('Removed');
        } else {
          console.log('ERROR: ' + err);
        }
      })
    });
  }

  //Link routes and functions
  app.get('/moons', findAllMoons);          
  app.get('/moon/:id', findMoonById);       
  app.post('/moon', addMoon);               
  app.put('/moon/:id', updateMoon);
  app.delete('/moon/:id', deleteMoon);

}