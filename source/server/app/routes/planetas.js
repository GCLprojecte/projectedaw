//File: routes/planetas.js
module.exports = function(app) {

  var Planeta = require('../models/planeta.js');

  //GET - Devuelve todos los planetas de la BD
  findAllPlanetas = function(req, res) {
    Planeta.find(function(err, planetas) {
      if(!err) {
        console.log('GET /planetas')
        res.send(planetas);
      } else {
        console.log('ERROR: ' + err);
      }
    });
  };

  //GET - Devuelve el planeta con la ID especificada
  findPlanetaById = function(req, res) {
    Planeta.find({id : req.params.id}, function(err, planeta) {
      if(!err) {
        console.log('GET /planeta/' + req.params.id);
        res.send(planeta);
      } else {
        console.log('ERROR: ' + err);
      }
    });
  };

  //POST - Inserta un planeta en La BD
  addPlaneta = function(req, res) {
    console.log('POST');
    console.log(req.body);

    var planeta = new Planeta({

      _id         : req.body.id,
      id          : req.body.id,
      name        : req.body.name,
      composition : req.body.composition,
      distance    : req.body.distance,
      radius      : req.body.radius,
      type        : req.body.type,
      atmosfear   : req.body.atmosfear,
      surface     : req.body.surface,
      temperature : req.body.temperature,
      moons       : req.body.moons

    });

    planeta.save(function(err) {
      if(!err) {
        //res.send("Planeta creado");
        console.log('Planeta creado');
      } else {
        //res.send("ERROR" + err);
        console.log('ERROR: ' + err);
      }
    });

    res.send(planeta);
  };

  //PUT - Actualiza un registro de la id especificada
  updatePlaneta = function(req, res) {
    Planeta.findById(req.params.id, function(err, planeta) {
      
      planeta.id          = req.body.id,
      planeta.name        = req.body.name,
      planeta.composition = req.body.composition,
      planeta.distance    = req.body.distance,
      planeta.radius      = req.body.radius,
      planeta.type        = req.body.type,
      planeta.atmosfear   = req.body.atmosfear,
      planeta.surface     = req.body.surface,
      planeta.temperature = req.body.temperature,
      planeta.moons       = req.body.moons

      planeta.save(function(err) {
        if(!err) {
          console.log('Planeta Actualizado');
        } else {
          console.log('ERROR: ' + err);
        }
        res.send(planeta);
      });
    });
  }

  //DELETE - Elimina el planeta de la id especificada
  deletePlaneta = function(req, res) {
    Planeta.findById(req.params.id, function(err, planet) {
      planet.remove(function(err) {
        if(!err) {
          console.log('Removed');
        } else {
          console.log('ERROR: ' + err);
        }
      })
    });
  }

  //Link routes and functions
  app.get('/planetas', findAllPlanetas);          //OK
  app.get('/planeta/:id', findPlanetaById);       //OK
  app.post('/planeta', addPlaneta);               //OK
  app.put('/planeta/:id', updatePlaneta);
  app.delete('/planeta/:id', deletePlaneta);

}