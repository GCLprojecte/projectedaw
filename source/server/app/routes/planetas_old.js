//File: routes/planetas.js
module.exports = function(app) {

  var Planeta = require('../models/planeta.js');

  //GET - Devuelve todos los planetas de la BD
  findAllPlanetas = function(req, res) {
    Planeta.find(function(err, planetas) {
      if(!err) {
        console.log('GET /planetas')
        res.send(planetas);
      } else {
        console.log('ERROR: ' + err);
      }
    });
  };

  //GET - Devuelve el planeta con la ID especificada
  findById = function(req, res) {
    Planeta.findById(req.params.id, function(err, planeta) {
      if(!err) {
        console.log('GET /planeta/' + req.params.id);
        res.send(planeta);
      } else {
        console.log('ERROR: ' + err);
      }
    });
  };

  //POST - Inserta un planeta en La BD
  addPlaneta = function(req, res) {
    console.log('POST');
    console.log(req.body);

    var planeta = new Planeta({
      user: req.body.user,
      nombre: req.body.nombre,
      gold: req.body.gold
    });

    planeta.save(function(err) {
      if(!err) {
        console.log('Planeta creado');
      } else {
        console.log('ERROR: ' + err);
      }
    });

    res.send(planeta);
  };

  //PUT - Actualiza un registro de la id especificada
  updatePlaneta = function(req, res) {
    Planeta.findById(req.params.id, function(err, planeta) {
      planeta.user         = req.body.user;
      planeta.nombre       = req.body.nombre;
      planeta.gold         = req.body.gold;

      planeta.save(function(err) {
        if(!err) {
          console.log('Planeta Actualizado');
        } else {
          console.log('ERROR: ' + err);
        }
        res.send(planeta);
      });
    });
  }

  //DELETE - Elimina el planeta de la id especificada
  deletePlaneta = function(req, res) {
    TVShow.findById(req.params.id, function(err, planet) {
      planet.remove(function(err) {
        if(!err) {
          console.log('Removed');
        } else {
          console.log('ERROR: ' + err);
        }
      })
    });
  }

  //HolaMundo
  /*saludar = function(req, res) {
    res.send("Hola Mundo");
  };*/

  //Link routes and functions
  //app.get('/', saludar);
  app.get('/planetas', findAllPlanetas);
  app.get('/planeta/:id', findById);
  app.post('/planeta', addPlaneta);
  app.put('/planeta/:id', updatePlaneta);
  app.delete('/planeta/:id', deletePlaneta);

}