//File: routes/galaxy.js
module.exports = function(app) {

  var Planeta = require('../models/galaxy.js');

  //GET - Devuelve todos los galaxias de la BD
  findAllGalaxias = function(req, res) {
    Planeta.find(function(err, galaxias) {
      if(!err) {
        console.log('GET /galaxias')
        res.send(galaxias);
      } else {
        console.log('ERROR: ' + err);
      }
    });
  };

  //GET - Devuelve el galaxy con la ID especificada
  findGalaxyById = function(req, res) {
    Galaxy.find({id : req.params.id}, function(err, galaxy) {
      if(!err) {
        console.log('GET /galaxy/' + req.params.id);
        res.send(galaxy);
      } else {
        console.log('ERROR: ' + err);
      }
    });
  };

  //POST - Inserta un galaxy en La BD
  addGalaxy = function(req, res) {
    console.log('POST');
    console.log(req.body);

    var galaxy = new Galaxy({

      _id         		: req.body.id,
      id          		: req.body.id,
      name        		: req.body.name,
      composition 		: req.body.composition,
      radius      		: req.body.radius,
      type        		: req.body.type,
      coords  	  		: req.body.coords,
      fleets      		: req.body.fleets,
      galacticObjects 	: req.body.galacticObjects

    });

    galaxy.save(function(err) {
      if(!err) {
        //res.send("Galaxy creado");
        console.log('Galaxy creado');
      } else {
        //res.send("ERROR" + err);
        console.log('ERROR: ' + err);
      }
    });

    res.send(galaxy);
  };

  //PUT - Actualiza un registro de la id especificada
  updateGalaxy = function(req, res) {
    Planeta.findById(req.params.id, function(err, galaxy) {
      
      galaxy.id          		= req.body.id,
      galaxy.name        		= req.body.name,
      galaxy.composition 		= req.body.composition,
      galaxy.radius      		= req.body.radius,
      galaxy.type       		= req.body.type,
      galaxy.coords  	 		= req.body.coords,
      galaxy.fleets    		 	= req.body.fleets,
      galaxy.galacticObjects 	= req.body.galacticObjects

      galaxy.save(function(err) {
        if(!err) {
          console.log('Galaxy Actualizado');
        } else {
          console.log('ERROR: ' + err);
        }
        res.send(galaxy);
      });
    });
  }

  //DELETE - Elimina el planeta de la id especificada
  deleteGalaxy = function(req, res) {
    Planeta.findById(req.params.id, function(err, galaxy) {
      galaxy.remove(function(err) {
        if(!err) {
          console.log('Removed');
        } else {
          console.log('ERROR: ' + err);
        }
      })
    });
  }

  //Link routes and functions
  app.get('/galaxias', findAllGalaxias);         
  app.get('/galaxy/:id', findGalaxyById);      
  app.post('/galaxy', addPGalaxy);             
  app.put('/galaxy/:id', updateGalaxy);
  app.delete('/galaxy/:id', deleteGalaxy);

}