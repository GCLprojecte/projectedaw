module.exports = function(app) {

  var unijs = require('../modules/god.js');

  var BDJson = require('../models/filejson.js');

  var Universo = require('../models/universe.js');

//Funcion para guardar el universo
function saveUni(text){
    //Creacion del objeto
    var universe = new Universo(
      {
        galaxies: text.galaxies
      }
    );
    universe.save(function(err) {
      if(!err) {
        console.log('Universo creado');
      } else {
        console.log('ERROR: ' + err);
      }
    });
  }

  //GET - FUNCION PARA MOSTRAR CONTENIDO JSON
  unijson = function (req, res) {
    var textjson = unijs.Universe.create();

    var universe = new Universo({

      galaxies : textjson.galaxies

    });

    console.log(textjson);

    console.log(textjson.galaxies.length);

    console.log(textjson.galaxies[0].coords.x);

    res.send(universe); 
  };

  //GET - FUNCION PARA MOSTRAR CONTENIDO JSON
  unijson2 = function (req, res) {
    var textjson = unijs.Universe.create();

    for (var i = 0; i < textjson.galaxies.length; i++)
    {
      var galax_universo = [];
      var comp = [];
      var cordx = textjson.galaxies[i].coords.x;
      var cordy = textjson.galaxies[i].coords.y;

      
      for (var j=0; j < textjson.galaxies[i].composition.length; j++)
      {
        comp.push(textjson.galaxies[i].composition[j]);
      }

      var galaxiaarr = {
        _id             : textjson.galaxies[i].id,
        id              : textjson.galaxies[i].id,
        name            : textjson.galaxies[i].name,
        composition     : comp,
        radius          : textjson.galaxies[i].radius,
        type            : textjson.galaxies[i].type,
        coords          : { x : cordx, y : cordy},
        galacticObjects : []
      };

      galax_universo[i] = galaxiaarr;

    }

    var universe = new Universo({

       galaxies    : galax_universo

      });

    res.send(universe);
  };


 //GET - Devuelve el unvierso con la ID especificada
  findUniverso= function(req, res) {
    Universo.find({},function(err, universe) {
      if(!err) {
        console.log('GET /universe/');
        res.send(universe);
      } else {
        console.log('ERROR: ' + err);
        var textjson = unijs.Universe.create();
        res.send(textjson);
        saveUni(textjson);
      }
    });
  };


  //POST - Inserta un universo en La BD
  addUniverso = function(req, res) {

    var universe = new Universo({

      galaxies    : req.body.galaxies

    });

    universe.save(function(err) {
      if(!err) {
        //res.send("Universo creado");
        console.log('Universo creado');
      } else {
        //res.send("ERROR" + err);
        console.log('ERROR: ' + err);
      }
    });

    res.send(universe);
  };

  /*PUT - Añade una galaxia introducida por parametro.
  addGalaxyToUniverse = function(req, res) {
    
      universe.update({id: 'req.params.id'}, $push { galaxies: 'galaxy'})

      universe.save(function(err) {
        if(!err) {
          console.log('Galaxia Añadida');
        } else {
          console.log('ERROR: ' + err);
        }
        res.send(universe);
      });
    });
  }*/

  //DELETE - Elimina el universo de la id especificada
  /*deleteUniverse = function(req, res) {
    Universo.findById(req.params.id, function(err, univ) {
      unvir.remove(function(err) {
        if(!err) {
          console.log('Eliminado');
        } else {
          console.log('ERROR: ' + err);
        }
      })
    });
  }*/

  //Link routes and functions
  app.get('/unijson', unijson);
  app.get('/unijson2', unijson2);
  app.get('/universe', findUniverso);
  app.post('/universe', addUniverso);   
  //app.put('/universe/:id', addGalaxyToUniverso);
  //app.delete('/universe/:id', deleteUniverso);

}