module.exports = function(app) {

  var fs = require('fs');

  var unijs = require('../modules/god.js');

  var BDJSON = require('../models/filejson.js');
  var Universo = require('../models/universe.js');
  var Galaxy = require('../models/galaxy.js');
  var galacticObject = require('../models/galacticObject.js');
  var SSolar = require('../models/sis_solar.js');
  var Planeta = require ('../models/planeta.js');
  var Moon = require ('../models/moon.js');




  //Devuelve el contenido del json
  getLang = function (req, res){

    var name = req.params.lang;
/*
    console.log(typeof name);

    var file = './' + name + '.json';

    fs.readFile(file, 'utf8', function (err, data) {
      if (err) {
        console.log('Error: ' + err);
        return;
      }
      else
      {
        data = JSON.parse(data);
        console.log(data);
        return data;
      }
    })
*/
    res.send(getConfig(name+".json"));
  };

function readJsonFileSync(filepath, encoding){

    if (typeof (encoding) == 'undefined'){
        encoding = 'utf8';
    }
    var file = fs.readFileSync(filepath, encoding);
    return JSON.parse(file);
}

function getConfig(file){

    var filepath = "./public/lang/" + file;
    return readJsonFileSync(filepath);
}

//Funcion para guardar el universo
crearjsonbd = function (req, res){
    //Creacion del objeto
    var univJson = unijs.Universe.create();

    var bdjson = new BDJSON({

      galaxies : univJson.galaxies

    });

    bdjson.save();

    createobjects(univJson);

    return bdjson;
  }

  //Funcion que crea todos los objetos y los guarda en la BBDD
  function createobjects (textjson){

    console.log("**********************CREADOR******************* ");

    //En cada galaxia
    for (var i = 0; i < textjson.galaxies.length; i++)
    {

      //En cada GalactObject
      for (var k = 0; k < textjson.galaxies[i].galacticObjects.length; k++)
      {
        var ssol_galob = [];    //Array de Sistemas Solares que tiene un galacticobject

        //Em cada sistema solar
        for ( var p = 0; p < textjson.galaxies[i].galacticObjects[k].length; p++)
        {

          var planet_ssol = [];   //Array de Planetas que tiene un Sistema Solar

          //En cada planeta
          for ( var  m = 0; m < textjson.galaxies[i].galacticObjects[k].planets.length; m++)
          {
            var moon_planet = [];   //Array de Lunas que tiene un Planeta

            //En cada luna
            for (var n = 0; n<textjson.galaxies[i].galacticObjects[k].planets[m].moons.length; n++)
            {
              var compm = [];
              for (var c = 0; c < textjson.galaxies[i].galacticObjects[k].planets[m].moons[n].compostion.length; c++)
              {
                comp[c] = textjson.galaxies[i].galacticObjects[k].planets[m].moons[n].compostion[c];
              }

              var tempmin = textjson.galaxies[i].galacticObjects[k].planets[m].moons[n].temperature.min;
              var tempmax = textjson.galaxies[i].galacticObjects[k].planets[m].moons[n].temperature.max;

              var luna = new Moon ({
                id        : textjson.galaxies[i].galacticObjects[k].planets[m].moons[n].id,
                name      : textjson.galaxies[i].galacticObjects[k].planets[m].moons[n].name,    
                composition   : compm,    
                distance    : textjson.galaxies[i].galacticObjects[k].planets[m].moons[n].distance,    
                radius      : textjson.galaxies[i].galacticObjects[k].planets[m].moons[n].radius,   
                type        : textjson.galaxies[i].galacticObjects[k].planets[m].moons[n].type,     
                atmosfear   : textjson.galaxies[i].galacticObjects[k].planets[m].moons[n].atmosfear, 
                surface     : textjson.galaxies[i].galacticObjects[k].planets[m].moons[n].surface,   
                temperature   : {         
                          "min":tempmin, 
                          "max":tempmax 
                          } 
              });

              luna.save();

              console.log(luna);

              moon_planet[n] = luna;
            }

            //Fin cada luna

            var compp = [];
             for (var cp = 0; cp < textjson.galaxies[i].galacticObjects[k].planets[m].compostion.length; cp++)
              {
                compp[cp] = textjson.galaxies[i].galacticObjects[k].planets[m].compostion[c];
              }

            var tempmin = textjson.galaxies[i].galacticObjects[k].planets[m].temperature.min;
            var tempmax = textjson.galaxies[i].galacticObjects[k].planets[m].temperature.max;


            var planet = new Planeta ({
              id        : textjson.galaxies[i].galacticObjects[k].planets[m].id,
              user      : textjson.galaxies[i].galacticObjects[k].planets[m].user,
              name      : textjson.galaxies[i].galacticObjects[k].planets[m].name,
              composition   : compp, 
              distance    : textjson.galaxies[i].galacticObjects[k].planets[m].distance,  
              radius      : textjson.galaxies[i].galacticObjects[k].planets[m].radius,
              type      : textjson.galaxies[i].galacticObjects[k].planets[m].type,  
              atmosfear     : textjson.galaxies[i].galacticObjects[k].planets[m].atmosfear,   
              surface     : textjson.galaxies[i].galacticObjects[k].planets[m].surface,    
              temperature   : {         //Temperatura
                        "min":tempmin, 
                        "max":tempmax 
                        },
              moons : moon_planet
            });

            console.log(planet);

            planet.save();

            planet_ssol[m] = planet;

            //Fin cada planeta
            
          }

          var cdx = textjson.galaxies[i].galacticObjects[k].coords.x;
          var cdy = textjson.galaxies[i].galacticObjects[k].coords.y;

          var sistem_solar = SSolar({
            id      : textjson.galaxies[i].galacticObjects[k].id, 
            coords    : { x: cdx, y: cgy }, 
            name      : textjson.galaxies[i].galacticObjects[k].name,
            type      : textjson.galaxies[i].galacticObjects[k].type,
            planets   : planet_ssol 
          });

          sistem_solar.save();

          ssol_galob[k] = sistem_solar;

          console.log("SSOL_GALOB");
          console.log(ssol_galob);

        }

        //Fin cada sistema solar

        var galob = new galacticObject ({
          solar_system : ssol_galob
        });

        galob.save();

      }

      //Fin cada galacticobject

      var galax_universo = [];
      var comp = [];
      var cordx = textjson.galaxies[i].coords.x;
      var cordy = textjson.galaxies[i].coords.y;

      
      for (var j = 0; j < textjson.galaxies[i].composition.length; j++)
      {
        comp.push(textjson.galaxies[i].composition[j]);
      }

      var galaxiaarr = new Galaxy ({
        id              : textjson.galaxies[i].id,
        name            : textjson.galaxies[i].name,
        composition     : comp,
        radius          : textjson.galaxies[i].radius,
        type            : textjson.galaxies[i].type,
        coords          : { x : cordx, y : cordy},
        galacticObjects : galob
      });

      galaxiaarr.save();

      galax_universo.push(galaxiaarr);

    }

    //Fin cada galaxia

    var universe = new Universo({

       galaxies    : galax_universo

      });

    console.log("UNIVERSE");
    console.log(universe);
    console.log("UNIVERSE.GALAXIES")
    console.log(universe.galaxies);
    console.log("UNIVERSE.GALAXIES[0].GALACTICOBJECTS")
    console.log(universe.galaxies[0].galacticObjects[0]);
  }


 //GET - Devuelve el planeta con la ID especificada
  findBDJson = function(req, res) {
    BDJSON.find(function(err, bdjson) {
      if(!err) {
        console.log('GET /BDJson' + bdjson);

        if(bdjson.length == 0)
        {
          console.log(bdjson.length);
          bdjson=crearjsonbd();
        }
        

        res.send(bdjson[0]);
      } 

      else {
        console.log('ERROR: ' + err);
        res.send('ERROR: ' + err);
      }
    });
  };

  

  //GET - Busca planeta sin owner y lo devuelve
  buscaPlaneta = function(req, res)
  {
    BDJSON.find(function(err, bdjson){
      var flag=0;
      for (var i = 0; i < bdjson.length; i++)
      {
        if(flag==1) 
        {
          break;
        }
        //console.log("*****BJDSON[I]******");
        //console.log(bdjson[i]);
        for (var j = 0; j < bdjson[i].galaxies.length; j++)
        {
           if(flag==1) 
          {
            break;
          }
          //console.log("*****BDJSON[I].GALAXIES[J]*****");
          //console.log(bdjson[i].galaxies[j]);
          for (var k = 0; k < bdjson[i].galaxies[j].galacticObjects.length; k++)
          {
            if(flag==1) 
            {
              break;
            }
            //console.log("GALACTICOBJECT" + k);
            //console.log(bdjson[i].galaxies[j].galacticObjects[k]);
            for (var p = 0; p < bdjson[i].galaxies[j].galacticObjects[k].planets.length; p++)
            {
               if(flag==1) 
              {
                break;
              }
              if(bdjson[i].galaxies[j].galacticObjects[k].planets[p].owner.length==0)
              {
                //console.log("**********owner vacio***********");
                bdjson[i].galaxies[j].galacticObjects[k].planets[p].owner="Nuevo user";
                flag=1;
              }
              //console.log("PLANETA "+p);
              //console.log(bdjson[i].galaxies[j].galacticObjects[k].planets[p]);
              
            }
          }
        }
      }

      BDJSON.findById(bdjson[0]._id, function (err, bdjson2)
      {
        bdjson2 = bdjson[0];

        bdjson2.save(function(err) {
          if(!err) {
            console.log('BDjson2 actualizado');
          } else {
            console.log('ERROR: ' + err);
          }
        });

      });

    });
  };
  


  //Link routes and functions
  app.get('/BDJson', findBDJson);
  app.get('/crearjson', crearjsonbd);
  app.get('/getlang/:lang', getLang);
  app.get('/buscaplaneta', buscaPlaneta);
}