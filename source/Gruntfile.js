
module.exports = function(grunt){
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        concat: {
            
            css:{
                src:['client/View/cssObjects/*'],
                dest: 'server/public/dist/css/objects.css'
            },
            dist: {
                options: {
                    separator: ";"
                },
                src:[
                'node_modules/three/three.js',
                'libs/*.js',
                'client/View/View.js',
                'client/View/*.js',
                'shared/Core/*.js',
                //'!shared/Core/god.js',
                'client/Controller/*.js'
                ],
                dest: 'server/public/dist/Universe.js'
            }
        },
        uglify:{
            options:{
                banner: "/*! Universe pre-alpha 0.01v minified*/"
            },
            dist: {
                files: {
                    'server/public/dist/Universe.min.js': ['<%= concat.dist.dest %>']
                }
            }
        },
        jshint:{
            files: ['Gruntfile.js', 'client/**/*.js', 'shared/**/*.js', 'test/**/*.js'],
            options: {
                globals: {
                    console: true,
                    module: true
                }
            }
        },
        watch:{
            files:['<%= concat.dist.src %>', '<%= concat.css.src %>'],
            tasks: ['jshint','yuidoc', 'concat']
        },
        parallel:{
            test:{
                tasks:[
                {
                    cmd: './node_modules/venus/bin/venus',
                    args: ['run','-t','test']
                },
                {
                    grunt:true,
                    args:['yuidoc']
                },
                {
                    cmd: 'sensible-browser',
                    args: ['http://localhost:2013']
                },
                {
                    cmd: 'sensible-browser',
                    args: ["../docs/api/index.html"]
                },
                {
                    grunt:true,
                    args:['watch']
                }]
            },
            pull:{
                tasks:[{
                    cmd: 'git',
                    args:['pull']
                }]
            }
        },
        yuidoc:
        {
        compile:
            {
            name: '<%= pkg.name %>',
            description: '<%= pkg.description %>',
            version: '<%= pkg.version %>',
            url: '<%= pkg.homepage %>',
            options:
                {
                    linkNatives: true,
                    selleck: true,
                    paths: ['./client', './shared'],
                    outdir: '../docs/api'
                }
            }
        }

    });

grunt.loadNpmTasks('grunt-contrib-uglify');
grunt.loadNpmTasks('grunt-contrib-jshint');
grunt.loadNpmTasks('grunt-contrib-watch');
grunt.loadNpmTasks('grunt-contrib-concat');
grunt.loadNpmTasks('grunt-parallel');
grunt.loadNpmTasks('grunt-contrib-yuidoc');


grunt.registerTask('default', ['jshint','concat', 'watch']);
grunt.registerTask('dist', ['jshint','concat', 'uglify']);
grunt.registerTask('sync', ['parallel:pull']);
grunt.registerTask('development', ['jshint','parallel:test']);


};
